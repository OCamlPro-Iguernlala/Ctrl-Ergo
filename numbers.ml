(******************************************************************************)
(** Ce fichier founit deux modules pour manipuler des entiers et des rationnels
    avec une precision arbitraire **)
(******************************************************************************)

module Z_ARITH = struct
  
  module ZZ = struct
    type t = Z.t

    let zero           = Z.zero
    let one            = Z.one
    let minus_one      = Z.minus_one
    let equal   t1 t2  = Z.equal t1 t2
    let compare t1 t2  = Z.compare t1 t2
    let compare_to_0 t = Z.sign t
    let is_zero t      = compare_to_0 t = 0
    let is_one  t      = equal t one
    let is_minus_one t = equal t minus_one
    let of_int n       = Z.of_int n
    let of_string s    = Z.of_string s
    let to_string t    = Z.to_string t
    let pgcd t1 t2     = 
      if is_zero t1 then t2
      else if is_zero t2 then t1
      else Z.gcd t1 t2

    let minus t = Z.neg t
    let div t1 t2 = Z.div t1 t2
    let div_rem t1 t2 = Z.div_rem t1 t2
    let mult t1 t2 = Z.mul t1 t2
    let sub t1 t2 = Z.sub t1 t2
    let add t1 t2 = Z.add t1 t2
    let abs t = Z.abs t
    let ppmc t1 t2     = try Z.lcm t1 t2 with Division_by_zero -> assert false
    let hash t = Z.hash t
    let sign t = Z.sign t

  end

  module QQ = struct
    type t = Q.t

    let zero           = Q.zero
    let one            = Q.one
    let minus_one      = Q.minus_one
    let inf            = Q.inf
    let compare t1 t2  = Q.compare t1 t2
    let compare_to_0 t = Q.sign t
    let equal t1 t2    = Q.equal t1 t2
    let is_zero t      = compare_to_0 t = 0
    let is_one  t      = equal t one
    let is_minus_one t = equal t minus_one
    let of_int n       = Q.of_int n
    let of_z z         = Q.make z ZZ.one
    let of_zz z1 z2    = Q.make z1 z2
    let of_string s    = Q.of_string s
    let to_string t    = Q.to_string t
    let add t1 t2      = Q.add t1 t2
    let sub t1 t2      = Q.sub t1 t2
    let mult t1 t2     = Q.mul t1 t2
    let div t1 t2      = Q.div t1 t2
    let minus t        = Q.neg t
    let abs t          = Q.abs t
    let numerator t    = Q.num t
    let denominator t  = Q.den t
    let is_integer t   = ZZ.is_one (denominator t)
    let inv t = 
      if ZZ.is_zero (numerator t) then raise Division_by_zero
      else Q.inv t
      
    let modulo t1 t2 = 
      assert (is_integer t1 && is_integer t2);
      {Q.num=Z.rem (numerator t1) (numerator t2); den = ZZ.one}

    (**** crado *)

    let applique f t = 
      Q.of_string (Num.string_of_num (f (Num.num_of_string (Q.to_string t))))

    let to_float t = 
      Num.float_of_num (Num.num_of_string (Q.to_string t))

    let floor t = applique Num.floor_num t 

    let ceiling t = applique Num.ceiling_num t 

    let sign t = Q.sign t
    let power t1 n = assert false
    let root_default t n = assert false
    let root_exces t n = assert false

    let min t1 t2 = Q.min t1 t2
      
  end
end

module Z = Z_ARITH.ZZ
module Q = Z_ARITH.QQ
  

