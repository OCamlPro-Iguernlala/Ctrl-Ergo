open Ast
open Format
open Options
open Env.Debug
open Env

(*module H = Weak.Make
  (struct
     type t = expr
     let hash = Hashtbl.hash
     let equal a b = a = b
  end)*)

module H = Hashtbl
    
let (vrai : (expr,unit) H.t)= H.create 10001
let (faux : (expr,unit) H.t)= H.create 10001

let dead_expr = ref false

let type_const t c kd = match kd with
  | Numeral -> t.ty <- Int
  | _       -> assert false

let type_var t v =
  let (fresh, val_opt, ty, nb_use) as vinfo = lookup_for_var v in
  incr nb_use;
  Env.Debug.lookup_for_var v vinfo;
  t.ty   <- ty;
  match val_opt, ty with
    | Some defn, Int  when !Options.subst_lets && Ast.is_simple defn ->
        t.desc <- defn.desc
    | Some defn, Bool when !Options.subst_flets && Ast.is_simple defn ->
      t.desc <- defn.desc
    | _  -> t.desc <- Var fresh
        
        
let rec type_expr t = match t.desc with
  | Const (c,kd) -> type_const t c kd
  | Var v        -> type_var t v 
  | Let (vbl,t') -> type_let t vbl t'
  | True | False -> ()
  | App ((And | Or), tl)           -> type_app_ac t tl Bool
  | App ((Add | Mult), tl)         -> type_app_ac t tl Int
  | App (Sub, tl)                  -> type_app_sub t tl
  | App ((Ge|Gt|Le|Lt) as op, tl)  -> type_app_rel t op tl 
  | App (Not, tl)                  -> type_not t tl
  | App (Eq,  tl)                  -> type_eq  t tl
  | App (Ite, tl)                  -> type_ite t tl

and type_app_ac t tl ty = 
  List.iter 
    (fun s -> type_expr s; if s.ty<>ty then error (Bad_type (ty, s.ty))) tl;
  t.ty <- ty

and type_app_sub t tl = 
  List.iter 
    (fun s -> type_expr s; if s.ty<>Int then error (Bad_type (Int, s.ty))) tl;
  match tl with 
    | [_] | [_;_] -> t.ty <- Int 
    | _ -> error (Wrong_number_of_arguments Sub)

and type_app_rel t op tl = 
    List.iter 
      (fun s -> type_expr s; if s.ty<>Int then error (Bad_type (Int, s.ty))) tl;
  match tl with
    | [_;_] -> t.ty <- Bool
    | _ -> error (Wrong_number_of_arguments op)
        
    
and type_not t tl = match tl with
  | [s] ->
      type_expr s; if s.ty <> Bool then error (Bad_type (Bool, s.ty));
      t.ty <- Bool
  | [] | _::_::_ -> error (Wrong_number_of_arguments Not)
        
        
and type_eq  t tl = 
  List.iter type_expr tl;
  t.ty <- Bool;
  match tl with
    | [{ty=Int }; {ty=Int }] -> ()
    | [{ty=Bool}; {ty=Bool}] -> ()
    | [{ty=ty1}; {ty=ty2}] -> error (Bad_type (ty1, ty2))
    | _ -> error (Wrong_number_of_arguments Eq)

and type_ite t tl = 
  if !dead_expr then type_ite_dead t tl else type_ite_aleave t tl

and type_ite_dead t tl =
  match tl with
    | [c;t1;t2] ->
        type_expr c;
        type_expr t1;
        type_expr t2;
        begin
          match c.ty, t1.ty, t2.ty with
            | Bool, Int,  Int  -> t.ty <- Int
            | Bool, Bool, Bool -> t.ty <- Bool
            | Int,  _   , _    -> error (Bad_type (Bool, Int))
            | Bool, ty1 ,  ty2 -> error (Bad_type (ty1, ty2))
            | Dummy, _  ,  _   -> error (Bad_type (Bool, Dummy))
        end
    | _ -> error (Wrong_number_of_arguments Ite)


and type_ite_aleave t tl =
  match tl with
    | [c;t1;t2] ->
        type_expr c;
        let nc = Env.mk_not c in
        if H.mem vrai c || H.mem faux nc then begin
          if !dtyping then 
            fprintf fmt "la branche ELSE %a sera ignoree@." Ast.print t2;
          type_expr t1;
          t.desc <- t1.desc;
          dead_expr := true;
          type_expr t2;
          dead_expr := false;
        end
        else if H.mem faux c || H.mem vrai nc then  begin
          if !dtyping then 
            fprintf fmt "la branche THEN %a sera ignoree@." Ast.print t1;
          dead_expr := true;
          type_expr t1;
          dead_expr := false;
          type_expr t2;
          t.desc <- t2.desc;
        end
        else begin
          H.add vrai c ();
          H.add faux nc ();
          type_expr t1;
          H.remove vrai c;
          H.remove faux nc;
          H.add faux c ();
          H.add vrai nc ();
          type_expr t2;
          H.remove faux c;
          H.remove vrai nc;
        end;
        begin
          match c.ty, t1.ty, t2.ty with
            | Bool, Int,  Int  -> t.ty <- Int
            | Bool, Bool, Bool -> t.ty <- Bool
            | Int,  _   , _    -> error (Bad_type (Bool, Int))
            | Bool, ty1 ,  ty2 -> error (Bad_type (ty1, ty2))
            | Dummy, _  ,  _   -> error (Bad_type (Bool, Dummy))
        end
    | _ -> error (Wrong_number_of_arguments Ite)


and type_let t vbl in_t =
  List.iter (fun (_,tv) -> type_expr tv) vbl;
  register_local_vars vbl;
  type_expr in_t;
  match in_t.ty with 
    | Bool -> 
        t.ty <- Bool; 
        begin match unregister_local_vars [in_t] vbl with
          | []     -> assert false
          | [in_t] -> t.desc <- in_t.desc
          | l      -> t.desc <- App(And, l)
        end
    | _    -> error (Bad_type (Bool, t.ty))
    

let type_assertions () = 
  Queue.iter type_expr assertions;
  H.clear vrai;
  H.clear faux;
  if !Options.pp_only >= 0 then begin
    Env.H.clear Env.gamma;
    Queue.clear Env.loc_vars;
  end
