open Ast_translator

type t 
val empty : t
val dummy_empty : t
  
  
val env_nform : t -> Combine.t * Explanation.t -> Combine.t * Explanation.t
val assume : t -> Stypes.atom Queue.t -> int -> int -> t

val light_assume : t -> Stypes.atom -> t

val init : Stypes.var Vec.t -> t
val learn : t -> t * (Explanation.t * Stypes.atom) list

val expensive_processing : t -> t

val print : t -> unit
