open Simplex
open Format
open Options
open Numbers
open Ast_translator
open Stypes

module Ex = Explanation
module I = Intervals
module A = Arith
module C = Combine
module Tp = Theory_propagation
module MD = C.MDec
module SD = MD.Set
module SCache = Simplex_cache

type bound_kind = Sup | Inf | Both | Diseq
type binfo = { int : I.t; kind : bound_kind; a:bool }

type t = 
    { bounds     : binfo MD.t;
      ub_bs    : SD.t MD.t
    }

let empty =
  { bounds = MD.empty;
    ub_bs  = MD.empty }


let merge ineqs1 ineqs2 = 
  { bounds = MD.union (fun _ -> assert false) ineqs1.bounds ineqs2.bounds;
    ub_bs = MD.union (fun _ -> assert false) ineqs1.ub_bs ineqs2.ub_bs }


let kind_to_string = function
  | Sup -> "Sup" | Inf -> "Inf" | Both -> "Both" | Diseq -> "Diseq"

let print {bounds=bounds} = 
  if !dla then begin
    fprintf fmt "---------- Bounds ---------------------------------@.";
    MD.iter 
      (fun x {int=int;kind=kd} -> 
        fprintf fmt "%a -> %a // %s@."
          C.print x I.print int (kind_to_string kd)
      ) bounds;
    (*fprintf fmt "---------- Used_by Bounds -------------------------@.";
      MD.iter 
      (fun x st -> 
      fprintf fmt "%a -> %a@." C.print x pset st
      )env.ub_bs;*)
    fprintf fmt "---------------------------------------------------@.@."
  end

let print_normalize_bounds_by_p_rs env p_rs =
  if !dla then begin
    let nb_r = List.length p_rs in
    let nb_b = MD.cardinal env.bounds in
    fprintf fmt "normalize %d bounds using %d rules@." nb_b nb_r;
  end


let merge_kinds kd kd0 = match kd,kd0 with
  | Sup, Inf | Inf, Sup | Both, _ | _, Both -> Both
  | Sup, Sup | Inf, Inf -> kd
  | Diseq, _ -> kd0
  |  _, Diseq -> kd
    
let swap_kind gcd kd = 
  if Z.sign gcd >= 0 then kd
  else match kd with Sup -> Inf | Inf -> Sup | _ -> kd

let remove_from_ub_bs rpx ub_bs = ub_bs
(*
  List.fold_left
    (fun ub_bs lf ->
      MD.add lf (SD.remove rpx (MD.find_default lf SD.empty ub_bs)) ub_bs
    )ub_bs (C.xleaves rpx)
*)

let add_to_ub_bs rpx ub_bs = ub_bs(*
  List.fold_left
    (fun ub_bs lf ->
      MD.add lf (SD.add rpx (MD.find_default lf SD.empty ub_bs)) ub_bs
    )ub_bs (C.xleaves rpx)
                                  *)


let solve_ineq env p ex = 
  let m, gcd = A.normal_form_pos p in
  let c = Q.div (Q.of_z p.c) (Q.of_z gcd) in
  let kd = if Z.sign gcd > 0 then Sup else Inf in
  if MD.is_empty p.m then 
    let cmp = Q.compare_to_0 c  in
    if kd=Sup && cmp > 0  || kd=Inf && cmp < 0 then 
      raise (Ex.Inconsistent ex);
    env, None
  else 
    let p = { m = m ; c = Z.zero } in
    let rpx = C.alien_of_arith p in
    let {int=int0; kind=kd0} =
      try MD.find rpx env.bounds
      with Not_found -> {int = I.undefined Ast.Int; kind=kd;a=true} in
    let new_int = match kd with
      | Sup -> I.new_borne_sup ex (Q.minus c) true int0
      | Inf ->I.new_borne_inf ex (Q.minus c) true int0
      | _ -> assert false
    in
    match I.is_point new_int with
      | Some(v,ex) ->
        assert (Q.is_integer v);
        let bounds = MD.remove rpx env.bounds in
        let ub_bs = remove_from_ub_bs rpx env.ub_bs in
        {bounds = bounds; ub_bs=ub_bs}, Some (rpx, Q.numerator v, ex)
      | None -> 
        let binfo ={int=new_int; kind=merge_kinds kd kd0;a=true} in
        let bounds = MD.add rpx binfo env.bounds in
        let ub_bs = add_to_ub_bs rpx env.ub_bs in
        {bounds=bounds; ub_bs=ub_bs}, None


let solve_diseq env (rpx, n, ex) = 
  (*Debug.solve_diseq rpx n ex;*)
  begin
    try match C.solve rpx n with
      | [] -> raise (Ex.Inconsistent ex)
      | _ -> ()
    with Unsolvable -> ()
  end;
  let rp = C.arith_of_alien rpx in
  if MD.is_empty rp.m then env, None
  else 
    let m, gcd = A.normal_form_pos rp in
    let cc = Q.div (Q.of_z rp.c) (Q.of_z gcd) in
    let rp = {m=m; c=Z.zero} in
    let rpx = C.alien_of_arith rp in
    let minus_c = Q.sub (Q.div (Q.of_z n) (Q.of_z gcd)) cc in
    let point = I.point minus_c Ast.Int ex in
    try
      let {int=int} as info = MD.find rpx env.bounds in
      let new_int = I.exclude point int in
      match I.is_point new_int with
        | None ->
          let bounds = MD.add rpx {info with int=new_int;a=true} env.bounds in
          let ub_bs = add_to_ub_bs rpx env.ub_bs in
          {bounds = bounds; ub_bs=ub_bs}, None
        | Some (n,exn) ->
          let bounds = MD.remove rpx  env.bounds in
          let ub_bs = remove_from_ub_bs rpx env.ub_bs in
          let env = {bounds = bounds; ub_bs = ub_bs} in
          assert (Z.is_one (Q.denominator n));
          env, Some (rpx, (Q.numerator n), exn)
    with Not_found -> 
      let undefined = I.undefined Ast.Int in
      let new_int = I.exclude point undefined in
      let info = {int=new_int; kind=Diseq;a=true} in
      let bounds = MD.add rpx info env.bounds in
      let ub_bs = add_to_ub_bs rpx env.ub_bs in
      {bounds = bounds; ub_bs = ub_bs}, None (*XXX*)

let trivial_model {bounds=bounds} =
  MD.for_all (fun px {int=int} -> I.doesnt_contain_0 int = None) bounds
    

let apply_sbt_on_bounds env sbt = 
  let apply_on acc = 
    List.fold_left
      (fun ((px,exp,mem) as acc) (p,(v,ex)) ->
        let new_px = C.subst p v px in
        if C.equal new_px px then acc else new_px, Ex.union exp ex, false
      )acc sbt
  in
  let bounds, ub_bs, eqs = 
    MD.fold
      (fun px {int=int;kind=kd} (bounds, ub_bs, eqs) ->
        let new_px, ex, mem = apply_on (px, Ex.empty, true) in
        if mem then bounds, ub_bs, eqs
        else begin
          let new_p = C.arith_of_alien new_px in
          let old_c = new_p.c in
          let new_m, gcd = A.normal_form_pos new_p in 
          let new_p = {m = new_m ; c = Z.zero} in
          let point = I.point (Q.minus (Q.of_z old_c)) Ast.Int ex in 
          let add_int = I.add int point in
          let scale_int = I.scale (Q.of_zz Z.one gcd) add_int in
          let scale_int = I.intersect scale_int scale_int in (* NORM *)
          let new_px = C.alien_of_arith new_p in
          let bounds = MD.remove px bounds in
          let ub_bs = remove_from_ub_bs px ub_bs in
          if MD.is_empty new_p.m then
            match I.doesnt_contain_0 scale_int with
              | Some ex -> 
                (* XXX explain_interval manquait *)
                let ex = Ex.union ex (I.explain_interval scale_int) in
                raise (Ex.Inconsistent ex)
              | None -> 
                (* trivial *)
                let bounds = MD.remove new_px bounds in
                let ub_bs = remove_from_ub_bs new_px ub_bs in
                bounds, ub_bs, eqs
          else
            let kd = swap_kind gcd kd in
            let info, ub_bs =
              try
                let {int=i0; kind=k0} = MD.find new_px bounds in
                {int=I.intersect i0 scale_int;kind=merge_kinds k0 kd;a=true},
                ub_bs
              with Not_found -> 
                {int=scale_int; kind=kd;a=true}, add_to_ub_bs new_px ub_bs
            in
            match I.is_point info.int with
              | None -> MD.add new_px info bounds, ub_bs, eqs
              | Some(n,ex) -> 
                assert (Z.is_one (Q.denominator n));
                let bounds = MD.remove new_px bounds in
                let ub_bs = remove_from_ub_bs new_px ub_bs in
                bounds, ub_bs, (new_px,Q.numerator n,ex)::eqs
                  
        end
      )env.bounds (env.bounds, env.ub_bs, [])
  in 
  {bounds=bounds; ub_bs=ub_bs}, eqs

let apply_sbt env sbt = 
  print_normalize_bounds_by_p_rs env sbt;
  Timer.Fm_simplex_apply_mapping_to_bounds_2.start();
  let env, eqs = apply_sbt_on_bounds env sbt in
  Timer.Fm_simplex_apply_mapping_to_bounds_2.stop();
  env, eqs

let case_split env = 
  (*Debug.print env;*)
  let acc = 
    MD.fold
      (fun x {int=int;a=assumed} acc -> 
        if assumed then acc
        else match I.finite_size int with
	  | None   -> acc
	  | Some s ->
	    if Q.compare s Q.one <= 0 then acc
	    else match acc with
	      | Some (s', _, _, _) when Q.compare s' s <= 0 -> acc
	      | _ ->  
                match I.borne_inf int with
                  | Some (n, ex) -> Some (s, x, n, ex)
                  | _ -> assert false
      ) env.bounds None 
  in
  match acc with 
    | Some (s, x, n, ex) -> 
      assert (Z.is_one (Q.denominator n));
      let n = Q.numerator n in
      (*Debug.case_split x n; *)
      let p = C.arith_of_alien x in
      assert (Z.is_zero p.c);
      Some (EQ(x,n), ex, s)
        
    | None -> 
      let acc = 
	MD.fold
          (fun x {int=int} acc -> 
            match I.finite_size int with
	      | None   -> acc
	      | Some s ->
		if Q.compare s Q.one <= 0 then acc
		else match acc with
		  | Some (s', _, _, _) when Q.compare s' s <= 0 -> acc
		  | _ ->  
                    match I.borne_inf int with
                      | Some (n, ex) -> Some (s, x, n, ex)
                      | _ -> assert false
          ) env.bounds None 
      in
      match acc with
        | Some (s, x, n, ex) -> 
          assert (Z.is_one (Q.denominator n));
          let n = Q.numerator n in
          (*Debug.case_split x n; *)
          let p = C.arith_of_alien x in
          assert (Z.is_zero p.c);
          Some (EQ(x,n), ex, s)
            
        | None -> 
          (*Debug.no_case_split ();*)
          None



(**************************************************************************)

let add_to_sum ld sum p_m =
  MD.fold
    (fun x c sum ->
      let lp, ln = try MD.find x sum  with Not_found -> [], [] in
      if Z.sign c > 0 then MD.add x ((ld, Q.of_z c) :: lp, ln) sum
      else MD.add x (lp, (ld, Q.of_z c) :: ln) sum
    )p_m sum

let generalized_fm_projection constrs = 
  List.fold_left
    (fun (sum, ctt, lds) (ld, (px0, p, ex)) ->
      if MD.is_empty p.m then assert false
      else 
        let sum = add_to_sum ld sum p.m in
        let ctt = (ld, Q.of_z p.c) :: ctt in
        let lds = (ld, Q.one) :: lds in
        sum, ctt, lds
    )(MD.empty,[],[]) constrs

let polynomials_bounding_pb sum ctt lambdas =
  let vars_elim_eqs =
    MD.fold (fun x (l1,l2) acc -> (x,(l1,l2, Q.zero)) :: acc) sum [] in
  let lds_gt_z = lambdas in
  ctt, vars_elim_eqs, lds_gt_z

let monomial_bounding_pb sum ctt lambdas x sum_x is_pos =
  let max_ctt, vars_elim, s_neq = polynomials_bounding_pb sum ctt lambdas in
  let lp, ln = sum_x in
  let coef_x =(x, (lp, ln, if is_pos then Q.minus_one else Q.one)) in
  max_ctt, coef_x :: vars_elim, s_neq

let explain vals constrs = 
  List.fold_left
    (fun expl (ld, (re,eps)) ->
      if Q.compare re Q.zero = 0 && 
        Q.compare eps Q.zero = 0 then expl (* XXX eps ? re ? *)
      else let _, _ , ex = List.assoc ld constrs in Ex.union expl ex
    )Ex.empty vals

let new_poly_bound env eqs px p bound expl is_le = 
  assert (not (MD.is_empty p.m));
  let {int=int} as info = MD.find px env.bounds in
  let rp = C.arith_of_alien px in
  (* XXX : besoin de debuggage *)
  (*fprintf fmt "bound=%s@." (Q.to_string bound);
    let p_bound = A.sub p (assert false)
  (*(P.create_const bound (P.type_info p))*) in*)
  
  let bb = Q.sub (Q.of_z p.c) bound in
  let new_int =
    let diff = A.sub rp p in
    if MD.is_empty diff.m then
      let b =  Q.sub (Q.of_z rp.c) bb in
      I.new_borne_inf expl b is_le int
    else
      let diff = A.add rp p in
      if MD.is_empty diff.m then
        let b = Q.add (Q.of_z rp.c) bb in
        I.new_borne_sup expl b is_le int
      else assert false
  in 
  match I.is_point new_int with
    | Some (n,ex) ->
      assert (Z.is_one (Q.denominator n));
      let bounds = MD.remove px env.bounds in
      let ub_bs = remove_from_ub_bs px env.ub_bs in
      {bounds = bounds; ub_bs = ub_bs}, (px, Q.numerator n, ex) :: eqs

    | _-> 
      (* no need to update ub_bs *)
      let bounds = MD.add px {info with int=new_int;a=false} env.bounds in 
      {env with bounds = bounds }, eqs


let new_monome_bound env eqs expl x vof is_pos is_le = 
  let {int=int} as info = 
    try 
      MD.find x env.bounds 
    with Not_found -> (* XXX *)
      let kd = if is_pos then Sup else Inf in
      {int = I.undefined Ast.Int; kind=kd;a=false}
  in
  let new_int =
    if is_pos then I.new_borne_sup expl (Q.minus vof) is_le int
    else I.new_borne_inf expl vof is_le int
  in 
  match I.is_point new_int with
    | Some (n,ex) -> 
      assert (Z.is_one (Q.denominator n));
      let bounds = MD.remove x env.bounds in
      let ub_bs = remove_from_ub_bs x env.ub_bs in
      {bounds = bounds; ub_bs = ub_bs}, (x, Q.numerator n, ex) :: eqs

    | _-> 
      (* no nned to update ub_bs *)
      let bounds = MD.add x {info with int=new_int;a=false} env.bounds in
      {env with bounds = bounds}, eqs

let cpt = ref 0
let tighten_polynomials env eqs sum ctt lambdas nb_constrs constrs is_le =
  let max_ctt, equas, s_neq = polynomials_bounding_pb sum ctt lambdas in
  let r_max_ctt, r_equas, r_s_neq = SCache.make_repr max_ctt equas s_neq in
  let sim_res = match SCache.already_registered r_max_ctt r_equas r_s_neq with
    | None -> 
      if !dsimplex then fprintf fmt "Simplex poly in@.";
      incr cpt;
      if !dsimplex then fprintf fmt "new simplex %d@." !cpt;
      let res = Simplex_Q.main max_ctt equas s_neq nb_constrs in
      if !dsimplex then fprintf fmt "Simplex poly out@.";
      SCache.register r_max_ctt r_equas r_s_neq !cpt res ; 
      res
    | Some (n, res, ctt') ->
      if SCache.MI.compare Q.compare r_max_ctt ctt' = 0 then begin
        if !dsimplex then fprintf fmt "reuse RESULTS of simplex %d@." n;
        res
      end
      else begin
        if !dsimplex then fprintf fmt "reuse  simplex %d@." n;
        let res = Simplex_Q.partial_restart res max_ctt in
        res
      end
  in
  (*Debug.print_parsed_answer sim_res;*)
  match sim_res with
    | Unsat _  | Eq_unsat -> env, eqs, false
    | Unbound {vof=vof;vals=vals} ->
      raise (Ex.Inconsistent (explain vals constrs))
    | Max {vof=(re,eps);vals=vals} -> (* XXX: parties reelles nulles *)
      assert (Q.is_zero re);
      let expl = explain vals constrs in
      let cmp = Q.compare eps Q.zero in
      if cmp > 0 && is_le then begin
        (*fprintf fmt "Inconsistent@.";*)
        raise(Ex.Inconsistent expl);
      end;
      if cmp >= 0 && not is_le then raise(Ex.Inconsistent expl);
      let env, eqs = 
	List.fold_left
	  (fun (env,eqs) (ld,(re_ld, eps_ld)) ->
	    assert (Q.is_zero re_ld);
	    if Q.is_zero eps_ld then env, eqs
	    else 
	      let (px0, p, _) = List.assoc ld constrs in
	      new_poly_bound env eqs px0 p (Q.div eps eps_ld) expl is_le
	  )(env,eqs) vals
      in
      let bounds = 
        List.fold_left
          (fun bounds (_,(px0, _, _)) ->
            try MD.add px0 {(MD.find px0 bounds) with a=false} bounds
            with Not_found -> bounds
          )env.bounds constrs
      in
      {env with bounds=bounds}, eqs, true

let tighten_monomial 
    env eqs x sum_x is_pos sum ctt lambdas nb_constrs constrs is_le = 
  if false || !dla then fprintf fmt "tighten_monomial %s%a@." 
    (if is_pos then "+" else "-") C.print x;
  let max_ctt, equas, s_neq =
    monomial_bounding_pb sum ctt lambdas x sum_x is_pos in
  let r_max_ctt, r_equas, r_s_neq = SCache.make_repr max_ctt equas s_neq in
  let sim_res = 
    match SCache.already_registered_mon x r_max_ctt r_equas r_s_neq with
      | None -> 
        if !dsimplex then fprintf fmt "Simplex poly in@.";
        incr cpt;
        if !dsimplex then fprintf fmt "new simplex %d@." !cpt;
        let res = Simplex_Q.main max_ctt equas s_neq nb_constrs in
        if !dsimplex then fprintf fmt "Simplex poly out@.";
        SCache.register_mon x r_max_ctt r_equas r_s_neq !cpt res ; 
        res
      | Some (n, res, ctt') ->
        if SCache.MI.compare Q.compare r_max_ctt ctt' = 0 then begin
          if !dsimplex then fprintf fmt "reuse RESULTS of simplex %d@." n;
          res
        end
        else begin
          if !dsimplex then fprintf fmt "reuse  simplex %d@." n;
          let res = Simplex_Q.partial_restart res max_ctt in
          res
        end
  in
  (*Debug.print_parsed_answer sim_res;*)
  match sim_res with
    | Unsat _ | Eq_unsat -> env, eqs
    | Unbound {vof=vof;vals=vals} -> 
      raise (Ex.Inconsistent (explain vals constrs))
    | Max {vof=vof,eps; vals=vals} -> (* XXX: parties avec eps nulles *)
      assert (Q.is_zero eps);
      let expl = explain vals constrs in
      new_monome_bound env eqs expl x vof is_pos is_le

let tighten_monomials env eqs sum ctt lds nb_constrs constrs is_le = 
  MD.fold
    (fun x sum_x (env,eqs) ->
      let sum = MD.remove x sum in
      let env, eqs = 
	tighten_monomial 
          env eqs x sum_x true sum ctt lds nb_constrs constrs is_le in
      tighten_monomial
        env eqs x sum_x false sum ctt lds nb_constrs constrs is_le
    )sum (env, eqs)
    
let refine env constrs nb_constrs is_le = 
  if !dla then fprintf fmt "debut de refine@.";
  let sum, ctt, lambdas = generalized_fm_projection constrs in
  let res = if MD.is_empty sum then env, []
    else 
      let env, eqs, tigh_mons = 
        tighten_polynomials env [] sum ctt lambdas nb_constrs constrs is_le in
      if !Options.tighten_vars && tigh_mons then 
        tighten_monomials env eqs sum ctt lambdas nb_constrs constrs is_le
      else env, eqs
  in
  if !dla then fprintf fmt "fin refine@.";
  res

(**************************************************************************)

let add_ineq_sup (cpt,assumed) rx int constrs = 
  incr cpt;
  match I.borne_sup int with
    | None -> assert false
    | Some(bs,ex) ->
      let exi = I.explain_interval int in
      let ex = Ex.union exi ex in
      let p = C.arith_of_alien rx in
      assert (Z.is_one (Q.denominator bs));
      let p' = {p with c = Z.sub p.c (Q.numerator bs)} in
      let constr = rx, p' , ex in
      (!cpt, assumed, constr)::constrs
        
let add_ineq_inf (cpt,assumed) rx int constrs = 
  incr cpt;
  match I.borne_inf int with
    | None -> assert false
    | Some(bi,ex) ->
      let exi = I.explain_interval int in
      let ex = Ex.union exi ex in
      let p = C.arith_of_alien rx in
      assert (Z.is_one (Q.denominator bi));
      let p' = A.sub {m=MD.empty; c=Q.numerator bi} p in
      let constr = rx, p', ex in
      (!cpt, assumed, constr)::constrs

let split_constrs constrs = 
  if false then fprintf fmt "in split_constrs@.";
  let l, all_lvs =
    List.fold_left
      (fun (acc, all_lvs) ((n,b,(px,p,ex)) as it) ->
        let lvs = 
          List.fold_left 
            (fun acc e -> SD.add e acc) SD.empty (C.xleaves px) in
        ([it], lvs) :: acc , SD.union lvs all_lvs
      )([], SD.empty) constrs
  in
  let ll =
    SD.fold
      (fun x l ->
        let lx, l_nx = List.partition (fun (_,s) -> SD.mem x s) l in
        match lx with
          | [] -> assert false
          | e:: lx ->
            let elx = 
              List.fold_left
                (fun (l, lvs) (l', lvs') -> 
                  List.rev_append l l', SD.union lvs lvs') e lx in
            elx :: l_nx
      ) all_lvs l
  in 
  if !dla then fprintf fmt "# nb de blocs avant: %d@." (List.length ll);
  let ll=
    List.filter 
      (fun (l,_) ->
        List.exists 
          (fun (_,assumed,(_,_,_)) -> assumed)l) ll
  in
  if !dla then fprintf fmt "# nb de blocs apres: %d@." (List.length ll);
  let ll = 
    List.fast_sort (fun (a,_) (b,_) -> List.length a - List.length b) ll in
  let ll = 
    List.rev_map
      (fun (constrs, _) ->
        let cpt = ref 0 in
        List.rev_map 
          (fun (_,_,(px,p,ex)) -> 
            incr cpt; !cpt, (px,p,ex)) constrs
      )ll
  in
  if !dla then fprintf fmt "out split_constrs@.";
  ll

let reduced_constraints env = 
  let cpt = ref 0 in
  let constrs = MD.fold
    (fun rx {int=i; kind=kd; a=a} constrs ->
      assert(if I.is_point i <> None then begin
        fprintf fmt "%a -> %a //%s!!@."
          C.print rx I.print i (kind_to_string kd);
        false
      end
        else true
      );
      assert(not (MD.is_empty (C.arith_of_alien rx).m));
      match kd with
        | Sup  -> add_ineq_sup (cpt,a) rx i constrs
        | Inf  -> add_ineq_inf (cpt,a) rx i constrs
        | Both -> 
          add_ineq_sup (cpt,a) rx i (add_ineq_inf (cpt,a) rx i constrs)
        | Diseq -> 
          constrs
    )env.bounds []
  in
  constrs, !cpt


let dfs_fm env = 
  print env;
  let constrs, nb_constrs = reduced_constraints env in
  (*Timer.Expensive_processing_split_constrs.start();*)
  let blocs = split_constrs constrs in
  (*let blocs = [List.map (fun (a,b,c) -> a,c) constrs] in*)
  (*Timer.Expensive_processing_split_constrs.stop();*)
  List.fold_left
    (fun (env,eqs) constrs ->
      let nb_constrs = List.length constrs in
      (*Debug.reduced_block_constraints constrs;*)
      let env, eqs' = refine env constrs nb_constrs true in
      env, eqs' @ eqs
    )(env, []) blocs



let count_diseqs env = 
  let cpt = ref 0 in
  let cpt2 = ref 0 in
  let acc =
    MD.fold
      (fun px {int=int} acc -> 
        match I.diseqs int with
            [] -> acc
          | dis -> 
            incr cpt2;
            List.iter (fun _ -> incr cpt) dis;
            (px,dis) :: acc
      )env.bounds []
  in
  match !cpt2, !cpt, acc with
    | 1, 1, [(px,[((q1,ex1), (q2,ex2))])] -> 
      if !verbose then begin
        fprintf fmt "-----------------------------------@.";
        fprintf fmt "polys diseqs = %d@." !cpt2;
        fprintf fmt "total diseqs = %d@.@." !cpt;
        fprintf fmt "Yes@."; 
      end;
      Some (px, q1, ex1, q2, ex2)
    | _ -> 
      if !verbose then begin
        fprintf fmt "-----------------------------------@.";
        fprintf fmt "polys diseqs = %d@." !cpt2;
        fprintf fmt "total diseqs = %d@.@." !cpt;
        fprintf fmt "No@.";
      end;
      None
      
let dfs_fm env = 
  let make_inf p z = {p with c = Z.sub p.c z} in
  let make_sup p z = A.mult_const Z.minus_one {p with c = Z.sub p.c z} in
  let fct env p ex = 
    let env, op = solve_ineq env p ex in
    let env, eqs = dfs_fm env in
    env, eqs, op
  in
  let (env, equas) as res  = dfs_fm env in
  match count_diseqs env with
    | None -> res
    | Some (px, q1, ex1, q2, ex2) ->
      assert (Q.is_integer q1);
      assert (Q.is_integer q2);
      let p = C.arith_of_alien px in
      try 
        let p = make_inf p (Q.numerator q1) in
        ignore (fct env p ex1);
        res
      with Ex.Inconsistent dep -> 
        let p = make_sup p (Q.numerator q2) in
        let env, eqs, op = fct env p (Ex.union dep ex2) in
        let eqs = List.rev_append eqs equas in
        match op with
          | None   -> env, eqs
          | Some e -> env, e :: eqs


(***************************************************************************)


(*
  let assume_atom a = 
  if !dsat || !dla then fprintf fmt "I assume %a@." pr_atom a

  let solve_eq px n ex = 
  if (* !dsat ||*)!dla then
  if !verbose then 
  fprintf fmt "solve_eq %a=%s : %a@." 
  C.print px (Z.to_string n) Ex.print ex
  else
  fprintf fmt "solve_eq %a=%s@." C.print px (Z.to_string n)

  let solve_diseq px n ex = 
  if (* !dsat ||*) !dla then 
  if !verbose then
  fprintf fmt "solve_diseq %a<>%s : %a@." 
  C.print px (Z.to_string n) Ex.print ex
  else
  fprintf fmt "solve_diseq %a<>%s@." C.print px (Z.to_string n)

  let print_couple fmt (re, eps) = 
  fprintf fmt "(%s , %s)" (Q.to_string re) (Q.to_string eps)
  
  let print_answer (vof,vals) = 
  fprintf fmt "vof = %a@." print_couple vof
(*List.iter
  (fun (l,v) ->
  fprintf fmt "  L(%d) -> %a@." l print_couple v
  )vals*)

  let print_parsed_answer answer =
  if !dla then
  match answer with
  | Unsat {vof=vof;vals=vals} -> 
  fprintf fmt "J'ai lu: Le probleme n'est pas faisable@.";
  print_answer (vof,vals)
  | Eq_unsat ->
  fprintf fmt "J'ai lu: Le probleme n'est pas faisable@."
  | Unbound {vof=vof;vals=vals} -> 
  fprintf fmt "J'ai lu: Le probleme n'est pas borne@.";
  print_answer (vof,vals)
  | Max {vof=vof;vals=vals}  -> 
  fprintf fmt "J'ai lu: Le probleme a une solution@.";
  print_answer (vof,vals)
  
  let print_lambda_poly s p =
  if !dla  then fprintf fmt "association: %s * %a@." s A.print p

  let reduced_block_constraints red_ineqs = 
  if !dla then begin
  fprintf fmt "Reduced_ineqs:@.";
  List.iter 
  (fun (lambda,(_,p,ex)) -> 
  fprintf fmt "%d) %a <= 0@." lambda A.print p)
  red_ineqs
  end
*)
(*
  let case_split x n =
  if !dla then 
  fprintf fmt "[case-split] %a = %s@." C.print x (Z.to_string n)

  let no_case_split () =
  if !dla then fprintf fmt "[case-split] nothing@."

  let assume_case_split a b ex is_eq =
  if !dla then 
  if is_eq then 
  fprintf fmt "assume_case_split %a = %s with %a@."
  C.print a (Z.to_string b) Ex.print ex
  else 
  fprintf fmt "assume_case_split %a <> %s with %a@."
  C.print a (Z.to_string b) Ex.print ex
*)
