(******************************************************************************)
(** Ce module contient l'environnement de parsing et de typage **)
(******************************************************************************)

open Format
open Options
open Ast
open Ast_translator

module H = Hashtbl

module Debug = struct

  type error = 
    | Bad_type of ty * ty
    | Var_already_defined of string
    | Var_not_defined of string
    | Wrong_number_of_arguments of op
    | Wrong_number_of_match_arguments
        
  type warning = 
    | Variable_never_used of string
        
  let error_aux e = match e with
    | Var_already_defined s ->
      fprintf fmt "Typing error : %s is already defined@." s
    | Var_not_defined s ->
      fprintf fmt "Typing error : %s is not defined@." s
    | Bad_type (ty, ty') ->
      fprintf fmt "Typing error : an expression should have type %s " 
        (str_of_type ty);
      fprintf fmt "but it has type %s@." 
        (str_of_type ty')
        
    | Wrong_number_of_arguments op -> 
      fprintf fmt 
        "Typing error : the operator %s is applied to too many arguments@."
        (str_of_op op)
    | Wrong_number_of_match_arguments -> 
      fprintf fmt 
        "Typing error : the operator ite is applied to too many arguments@."
        
  let error e = error_aux e; exit 1
    
  let warning_aux w = match w with
    | Variable_never_used v ->
      fprintf fmt "Warning: the variable %s is never used@." v

  let warning w = 
    warning_aux w;
    if !warnings_as_errors then exit 1
      
  let lookup_for_var s (fresh_s, val_opt, ty, nb_use) =
    if !dtyping then
      fprintf fmt "lookup for %s. %s:%s is returned and is used %d times@."
        s fresh_s (str_of_type ty) !nb_use
end
open Debug

let logic  = ref Unknown_logic
let status = ref Unknown
let (assertions: expr Queue.t) = Queue.create()
let (gamma : (string, string * expr option * ty * int ref) H.t) = H.create 10007
let loc_vars = Queue.create()

let set_status st = status := st

let set_logic  lg = logic  := lg
  
let mk_var s       = { ty = Dummy ; desc = Var s ; nb_ite = 0 }

let mk_let vbl e   =
  let n = List.fold_left (fun n (_,e) -> n + e.nb_ite ) e.nb_ite vbl in
  { ty = Dummy ; desc = Let(vbl,e); nb_ite = n }

let mk_true        = { ty = Bool  ; desc = True ; nb_ite = 0 }

let mk_false       = { ty = Bool  ; desc = False ; nb_ite = 0 }

let mk_const (s,k) = { ty = Dummy ; desc = Const (s,k) ; nb_ite = 0 }


let mk_not e = match e.desc with
  | App(Not,[e']) -> e'
  | _             -> { ty = Dummy ; desc = App(Not, [e]) ; nb_ite = e.nb_ite }
    

let mk_and el = 
  let n = List.fold_left (fun n e -> n + e.nb_ite )0 el in
  let el = 
    List.fold_left
      (fun acc e ->
        match e.desc with
          | App(And, l) -> List.rev_append l acc
          | _ -> e :: acc
      )[] el
  in
  { ty = Dummy ; desc = App(And, el) ; nb_ite = n }

let mk_or el = 
  let n = List.fold_left (fun n e -> n + e.nb_ite )0 el in
  let el = 
    List.fold_left
      (fun acc e ->
        match e.desc with
          | App(Or, l) -> List.rev_append l acc
          | _ -> e :: acc
      )[] el
  in
  { ty = Dummy ; desc = App(Or, el) ; nb_ite=n}

let mk_app op el = 
  match op with
    | Not | And | Or -> assert false
    | _ ->
      let n = if op = Ite then 1 else 0 in
      let n = List.fold_left (fun n e -> n + e.nb_ite) n el in
      { ty = Dummy ; desc = App(op, el) ; nb_ite = n }

  
let rec add_assertion a = 
  match a.desc with
    | App(And, l) -> List.iter add_assertion l
    | _ -> Queue.push a assertions

let register_global_var v ty = 
  if H.mem gamma v then error (Var_already_defined v);
  H.add gamma v (v, None, ty, ref 0)

let loc_counter = ref 0

let register_local_vars vbl =
  List.iter
    (fun (v, t) ->
      incr loc_counter;
       (*let fresh_v = v ^ "_" ^ (string_of_int !loc_counter) in*)
      let fresh_v = v in
      H.add gamma v (fresh_v, Some t, t.ty, ref 0);
      Queue.push (v, t.ty) loc_vars
    )vbl

let unregister_local_vars acc vbl = 
  List.fold_left
    (fun acc (v, t) ->
      let fresh_v, t', ty, nb_use = H.find gamma v in (* t = t' *)
      let tfv = { ty = ty; desc = Var fresh_v ; nb_ite = 0 } in
      H.remove gamma v;
      if !nb_use = 0 then begin
        warning (Variable_never_used v);
        acc
      end
      else 
        match ty with
          | Int  ->
            if !subst_lets && Ast.is_simple t then acc 
            else { desc = App(Eq,[tfv;t]); ty = Bool ; nb_ite = t.nb_ite } :: acc
          (*begin 
             Queue.push { desc = App(Eq,[tfv;t]); ty = Bool } assertions;
             acc
             end*)
          | Bool -> 
            if !subst_flets && Ast.is_simple t then acc 
            else { desc = App(Eq,[tfv;t]); ty = Bool ; nb_ite = t.nb_ite} :: acc
           (*begin
             Queue.push { desc = App(Eq,[tfv;t]); ty = Bool } assertions;
             acc
             end*)
          | Dummy -> assert false
    ) acc vbl
    


let lookup_for_var s = 
  try H.find gamma s with Not_found -> error (Var_not_defined s)


(************************************************************************)

let output_decls fmt =
  fprintf fmt "(set-logic %s)@."  (str_of_logic !logic);
  fprintf fmt "(set-info :status %s)@." (str_of_status !status);
  
  fprintf fmt "@.;; Global vars declarations:@.";
  fprintf fmt   ";;;;;;;;;;;;;;;;;;;;;;;;;;;;@.@.";
  H.iter
    (fun _ (s, _, ty, _) ->
      fprintf fmt "(declare-fun %s () %s)@." s (str_of_type ty)) gamma;

  fprintf fmt "@.;; Local vars declarations:@.";
  fprintf fmt   ";;;;;;;;;;;;;;;;;;;;;;;;;;;;@.@.";
  Queue.iter
    (fun (s,ty) ->
      fprintf fmt "(declare-fun %s () %s)@." s (str_of_type ty)) loc_vars


let output_parsed_ast () =
  let fmt = std_formatter in
  output_decls fmt;
  fprintf fmt "@.;; Initial assertions:@.";
  fprintf fmt   ";;;;;;;;;;;;;;;;;;;;;;@.@.";
  Queue.iter (fprintf fmt "(assert %a)@." print) assertions;
  fprintf fmt "@.(check-sat)@.";
  fprintf fmt "(exit)@."
    
let output_typed_ast () =
  let fmt = std_formatter in
  output_decls fmt;
  fprintf fmt "@.;; Initial assertions:@.";
  fprintf fmt   ";;;;;;;;;;;;;;;;;;;;;;@.@.";
  Queue.iter (fprintf fmt "(assert %a)@." print) assertions;
  fprintf fmt "@.(check-sat)@.";
  fprintf fmt "(exit)@."
    
let output_non_hashed_form print_form form =
  let fmt = std_formatter in
  output_decls fmt;
  fprintf fmt "@.;; Non hashed form:@.";
  fprintf fmt   ";;;;;;;;;;;;;;;;;;;@.@.";
  fprintf fmt "(assert @.%a)@." print_form form;
  fprintf fmt "@.(check-sat)@.";
  fprintf fmt "(exit)@."

let output_hashed_form print_form form =
  let fmt = std_formatter in
  output_decls fmt;
  fprintf fmt "@.;; Hashed form:@.";
  fprintf fmt   ";;;;;;;;;;;;;;;;;;;@.@.";
  fprintf fmt "(assert @.%a)@." print_form form;
  fprintf fmt "@.(check-sat)@.";
  fprintf fmt "(exit)@."
  
  
module S = Set.Make(struct type t=Stypes.atom let compare=Stypes.cmp_atom end)

let output_proxies_decls fmt proxies =
  fprintf fmt "@.;; Proxies declarations:@.";
  fprintf fmt   ";;;;;;;;;;;;;;;;;;;;;;;;;;;;@.@.";
  let _ = 
    List.fold_left
      (fun s a ->
        if S.mem a s then s
        else
          let _ = fprintf fmt "(declare-fun %a () Bool)@." Stypes.pr_atom a in
          S.add a s
      ) S.empty proxies
  in
  ()

let output_cnf unit non_unit proxies =
   let fmt = std_formatter in
   output_decls fmt;
   output_proxies_decls fmt proxies;
   fprintf fmt "(assert (and @.";
   List.iter
     (fun cl -> match cl with
       | [a] -> fprintf fmt "%a@." Stypes.pr_atom a
       | _   -> assert false
     )unit;
   List.iter
     (fun cl ->
       match cl with
           [] | [_] -> assert false
         | a::l->
           fprintf fmt "(or %a" Stypes.pr_atom a;
           List.iter (fprintf fmt " %a" Stypes.pr_atom) l;
           fprintf fmt ")@.";
     )non_unit;
   fprintf fmt "))@.";
   fprintf fmt "@.(check-sat)@.";
   fprintf fmt "(exit)@."

