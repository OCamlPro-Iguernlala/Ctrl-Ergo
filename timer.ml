(******************************************************************************)
(** Ce module contient des modules permettant de profiler des fonctions
    bien cibl�es **)
(******************************************************************************)

open Unix

module General : sig
  val start : unit -> unit
  val get   : unit -> float
end = struct
  let tmp = ref 0.
  let start () = tmp := (times()).tms_utime
  let get () = (times()).tms_utime -. !tmp
end

module type S = sig
  val start : unit -> unit
  val stop  : unit -> unit
  val get   : unit -> float
end

module M (X:sig end) : S = struct
  let acc = ref 0.
  let tmp = ref 0.
  let start () = tmp := (times()).tms_utime
  let stop ()  = acc := !acc +. ((times()).tms_utime -. !tmp); tmp := 0.
  let get ()   = if !tmp=0. then !acc else !acc +. ((times()).tms_utime -. !tmp)
end

module E = struct end
module Simplex_main = M(E)
module CC_assume = M(E)
module CC_expensive_processing = M(E)
module Solver_minimize_dep = M(E)
module Fm_simplex_apply_mapping_to_rs = M(E)
module Fm_simplex_apply_mapping_to_bounds_2 = M(E)
module Fm_simplex_split_constrs = M(E)
module Fm_simplex_clustering = M(E)
module Tp_apply_mapping = M(E)
