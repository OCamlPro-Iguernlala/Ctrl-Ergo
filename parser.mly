%{ 
  open Env
  open Ast
  open Options
  open Format
%}



%start parse

/* general */
%token EXCLIMATIONPT
%token UNDERSCORE
%token AS
%token EXISTS
%token FORALL
%token LET
%token INT
%token BOOL
%token TRUE
%token FALSE

%token PLUS
%token MINUS
%token TIMES
%token GE
%token GT
%token LE
%token LT
%token NOT
%token EQ
%token ITE
%token LET
%token AND
%token OR



/* commands */
%token SETLOGIC
%token QF_LIA
%token SETOPTION
%token SETINFO
%token DECLARESORT
%token DEFINESORT
%token DECLAREFUN
%token DEFINEFUN
%token PUSH
%token POP
%token ASSERT
%token CHECKSAT
%token GETASSERT
%token GETPROOF
%token GETUNSATCORE
%token GETVALUE
%token GETASSIGN
%token GETOPTION
%token GETINFO
%token EXIT
%token SAT
%token UNSAT
%token UNKNOWN
%token STATUS
/* Other tokens */
%token LPAREN
%token RPAREN
%token EOF 

%token <string> NUMERAL
%token <string> DECIMAL
%token <string> HEXADECIMAL
%token <string> BINARY
%token <string> STRINGLIT
%token <string> KEYWORD
%token <string> SYMBOL


%type <unit> parse
%%

parse:
| commands { $1 }

commands:
| command commands { $1 }
| EOF { () }


command:
| LPAREN DECLAREFUN SYMBOL LPAREN RPAREN sort RPAREN  
    { register_global_var $3 $6 }
| LPAREN SETLOGIC QF_LIA RPAREN       { set_logic Qf_lia }
| LPAREN SETINFO STATUS status RPAREN { $4 }
| LPAREN SETINFO attribute RPAREN     { () }
| LPAREN ASSERT term RPAREN { add_assertion $3 }
| LPAREN CHECKSAT RPAREN    {()}
| LPAREN EXIT RPAREN        {()}

status:
| SAT     { set_status Sat }
| UNSAT   { set_status Unsat }
| UNKNOWN { (* default value *) }


attribute: 
  {()}
| KEYWORD {assert false}
| KEYWORD attributevalue 
      {
        if !dtyping then fprintf fmt "KEYWORD2: TODO@.";
      }

attributevalue:
| specconstant  { () }
| SYMBOL        { () }


sort:
| INT  { Int}
| BOOL { Bool }
      
specconstant:
| DECIMAL       { $1, Decimal }
| NUMERAL       { $1, Numeral }
| STRINGLIT     { $1, String  }
| HEXADECIMAL   { $1, Hexa    }
| BINARY        { $1, Binary  }

term_list: 
| term { [$1] }
| term term_list { $1 :: $2}

term:
| specconstant                                         { mk_const $1  }
| SYMBOL                                               { mk_var $1    }
| TRUE                                                 { mk_true  }
| FALSE                                                { mk_false }
| LPAREN LET LPAREN varbinding_list RPAREN term RPAREN { mk_let $4 $6 }
| LPAREN app term_list RPAREN                          { mk_app $2 $3 }
| LPAREN term RPAREN                                   { $2 }
| LPAREN AND term_list RPAREN                          { mk_and $3 }
| LPAREN OR term_list RPAREN                           { mk_or $3 }
| LPAREN NOT term RPAREN                               { mk_not $3 }

varbinding_list: 
| varbinding                 { [$1]     }
| varbinding varbinding_list { $1 :: $2 }

varbinding: LPAREN SYMBOL term RPAREN { ($2, $3) }

app:
| PLUS  { Add }
| MINUS { Sub }
| TIMES { Mult }
| GE    { Ge }
| GT    { Gt }
| LE    { Le}
| LT    { Lt }
| EQ    { Eq }
/*
| NOT   { Not }
| AND   { And }
| OR    { Or }
*/
| ITE   { Ite }

