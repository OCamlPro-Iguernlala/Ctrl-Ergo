(******************************************************************************)
(** Ce module contient les options de parsing et les flags de debuggage **)
(******************************************************************************)
open Format

let fmt = err_formatter

let usage = "usage: ctrl-ergo [options] file.smt2"

let early_pruning  = ref true
let subst_lets  = ref true
let subst_flets = ref true
let theory_propagation = ref true
let smt_pp = ref true

let learning = ref false

let clustering = ref false
let enable_theory = ref false

let warnings_as_errors = ref false
let theory_simplification = ref 0

let pp_only = ref 0

let parse_only = ref false
let type_only  = ref false
let simplify_only   = ref false
let clustering_only = ref false
let learning_only = ref false


let dsteps     = ref true
let dparsing   = ref false
let dtyping    = ref false
let dlifting   = ref false
let dlearning  = ref false
let dsat       = ref false
let dsimplex   = ref false
let dla        = ref false
let dsolve     = ref false
let dlia       = ref false
let dsplit     = ref false
let dtags      = ref false
let dheap      = ref false
let debug = ref false
let verbose = ref false
let dexplanations = ref false
let nb_case_splits = ref 0.
let size_case_splits = ref 0.
let nb_backtracks = ref 0.
let nb_backjumps = ref 0.
let tighten_vars = ref false
let dtp = ref false


let spec = [
  "-pp-only", Arg.Set_int pp_only, "set the pp level (0=diabled,  |1|=parsing, |2|=typing, |3|=ite elimination, |4|=hash-consing, |5|=early pruning, |6|=cnf conversion, '-'=output transformation's result)";
  

  "-parse-only", Arg.Set parse_only, " stop after parsing";
  "-type-only", Arg.Set type_only , " stop after typing";
  "-simplify-only", Arg.Set simplify_only , " stop after making cnf";
  "-clustering-only", Arg.Set clustering_only , " stop after clustering";
  "-learning-only", Arg.Set learning_only , " stop after learning";
  "-clustering", Arg.Set clustering , 
  " enable clustering (may be expensive)";
  "-learning", Arg.Set learning ,
  " enable learning";
  "-theory-propagation", Arg.Set theory_propagation,
  " enable dynamic learning (i.e. theory propagation or theory bcp)";
  "-theory-simplification", Arg.Set_int theory_simplification,
  " enable theory simplification when sat_solver simplification is invoked";
  "-early-pruning", Arg.Set early_pruning,
  " enable an early BCP before then CNF conversion";
  "-warnings-as-errors", Arg.Set warnings_as_errors,
  " treat warnings as errors";
  "-tighten-vars", Arg.Set tighten_vars,
  " compute the bounds of the variables";

  "-dsteps", Arg.Set dsteps , " print a message after each processing step";
  "-dtyping", Arg.Set dtyping , " debug the typing step";
  "-dtp", Arg.Set dtp , " debug the theory propagation module";
  "-dla", Arg.Set dla , " debug linear arithmetic";
  "-dsolve", Arg.Set dsolve , " debug linear arithmetic solver";
  "-dparsing", Arg.Set dparsing , " debug the parsing step";
  "-dlifting", Arg.Set dlifting , " debug the lifting step";
  "-dlearning", Arg.Set dlearning , " debug the learning step";
  "-debug", Arg.Set debug , "set the debug flag";
  "-dtags", Arg.Set dtags , "print the tags of hashconsed values";
  "-dexplanations", Arg.Set dexplanations, " debug flag for explanations";
  "-dsat", Arg.Set dsat, " debug flag for the sat-solver";
  "-dsimplex", Arg.Set dsimplex, " debug flag for the simplex module";
  "-dlia", Arg.Set dlia, " debug flag for the linear arithmetic modules";
  "-dheap", Arg.Set dheap, " debug flag for the heap";
  "-dsplit", Arg.Set dsplit, " debug flag for case-split";
  "-verbose", Arg.Set verbose, " debug flag";
  "-subst-lets", Arg.Set subst_lets, 
  " substitute definitions of simple (i.e. non-ite) terms";
  "-subst-flets", Arg.Set subst_flets, 
  " subst definitions of formulas";
  "-smt-pp", Arg.Set smt_pp, "";
]


let file = ref None

let set_file s =
  if Filename.check_suffix s ".smt2" then file := Some s
  else raise (Arg.Bad "no smt2 extension")
    
let _ = Arg.parse spec set_file usage


module Dsteps = struct

  module TG = Timer.General
  let step_parsing () = 
    if !dsteps then fprintf fmt "> (CPU %f s) Parsing done@." (TG.get())
        
  let step_typing () = 
    if !dsteps then fprintf fmt "> (CPU %f s) Typing done@." (TG.get())
        
  let step_conversion s =
    if !dsteps then
      fprintf fmt "> (CPU %f s) Conversion done (%s)@." (TG.get()) s

  let step_clustering () = 
    if !dsteps then fprintf fmt "> (CPU %f s) Clustering done@." (TG.get())
  
  let step_learning () = 
    if !dsteps then fprintf fmt "> (CPU %f s) Learning done@." (TG.get())
        
  let step_mk_cnf () =
    if !dsteps then fprintf fmt "> (CPU %f s) Mk-cnf from formulas@." (TG.get())

  let step_lifting () = 
    if !dsteps then fprintf fmt "> (CPU %f s) Lifting done@." (TG.get())
        
 
end

  
let set_gc_control = 
  let gc_c = Gc.get() in
  let gc_c =
    { gc_c with
        (*Gc.verbose = 0x3FF;*)
        Gc.minor_heap_size = 32000000; (* default 32000*)
        (*major_heap_increment = 0;    (* default 124000*)*)
        space_overhead = 80; (* default 80% des donnes vivantes *)
    } 
  in
  Gc.set gc_c
    
(*
  let set_gc_control = 
  let gc_c = Gc.get() in
  let gc_c =
  { gc_c with
(*Gc.verbose = 0x3FF;*)
  Gc.minor_heap_size = 320000; (* default 32000*)
  Gc.space_overhead = 80; (* default 80% des donnes vivantes *)
  } 
  in
  Gc.set gc_c
*)
