open Format
open Options
open Numbers
open Ast_translator

module Ex = Explanation
module A = Arith
module C = Combine
module MD = C.MDec
module SD = MD.Set

type t = 
    { rs         : (C.t * Z.t * Ex.t) MD.t;
      pending_rs : C.t list;
      ub_rs      : SD.t MD.t }
      
let empty = 
  { rs = MD.empty;
    pending_rs = [];
    ub_rs = MD.empty }

(** subst m * x in p by t *)
let subst_rat x m t p =
  try
    let c = MD.find x p.m in
    let p = { p with m = MD.remove x p.m } in
    let pgcd = Z.pgcd c m in
    let c' = Z.div c pgcd (** TODO better ?*) in
    let m' = Z.div m pgcd (** TODO better ?*) in
    m', A.add (A.mult_const c' (C.arith_of_alien t)) (A.mult_const m' p)
  with Not_found -> Z.one, p
    

let env_nform {rs=rs} (r,rm,ex_r) =
  let p, m, ex = 
    List.fold_left
      (fun ((newr,newrm,ex) as nw) x -> 
        try
	  let rc_x, m, ex_x = MD.find x rs in 
          let m',newr = subst_rat x m rc_x newr in
          newr, Z.mult m' newrm, Ex.union ex ex_x
        with Not_found -> nw) (C.arith_of_alien r, rm, ex_r) (C.xleaves r)
  in
  C.alien_of_arith p, m, ex


let pending_rs ({pending_rs=p_rs; rs=rs} as env) = 
  let p_rs = 
    List.rev_map
      (fun x -> try (x, MD.find x rs) with Not_found -> assert false) p_rs in
  p_rs, {env with pending_rs = []}

let print {rs=rs} = 
  if !dla then begin
    fprintf fmt "---------- Rewriting System -----------------------@.";
    MD.iter 
      (fun x (rx, z, ex) -> 
        fprintf fmt "%a -> %a / %s : %a@."
          C.print x C.print rx (Z.to_string z) Ex.print ex
      ) rs;
    (*fprintf fmt "---------- Used_by RS -----------------------------@.";
      MD.iter 
      (fun x st -> 
      fprintf fmt "%a -> %a@." C.print x pset st
      )env.ub_rs;*)
    fprintf fmt "---------------------------------------------------@.@."
  end

let add_to_ub_rs p v ub_rs = 
  List.fold_left
    (fun acc lf ->
      let rep_uses_lf = try MD.find lf acc with Not_found -> SD.empty in
      MD.add lf (SD.add p rep_uses_lf) acc
    )ub_rs (C.xleaves v)
    
let remove_from_ub_rs p v ub_rs =
  List.fold_left
    (fun acc lf ->
      let rep_uses_lf = try MD.find lf acc with Not_found -> SD.empty in
      MD.add lf (SD.remove p rep_uses_lf) acc
    )ub_rs (C.xleaves v)
    

(** solve if not empty *)
let solve_rat rp = 
  let xc = MD.choose rp.m in
  let x,c = MD.fold (fun x' c' ((x,c) as xc) ->
    if Z.compare (Z.abs c') (Z.abs c) < 0 then (x',c') else xc) rp.m xc in
  
  if Z.sign c < 0 then
    x, Arith {rp with m=MD.remove x rp.m}, Z.minus c
  else
    let p  = {rp with m=MD.remove x rp.m} in
    let p = A.mult_const Z.minus_one p in
    x, Arith p,  c

let apply_mapping_to_rs p v dv ex env = 
  let ub_rs = add_to_ub_rs p v env.ub_rs in
  let uses_p = try MD.find p ub_rs with Not_found -> SD.empty in
  let rs, ub_rs = 
    SD.fold
      (fun x  (rs, ub_rs) ->
        let rc_x, d, ex_x = try MD.find x rs with Not_found -> assert false in
        let rc =  C.arith_of_alien rc_x in
        let m, new_rc = subst_rat p dv v rc in
        let new_rc_x = C.alien_of_arith new_rc in
        if C.equal new_rc_x rc_x then rs, ub_rs
        else begin
          let new_d = Z.mult m d in
          let ex_u = Ex.union ex_x ex in
          let rs = MD.add x (new_rc_x, new_d, ex_u) rs in
          let ub_rs = remove_from_ub_rs x rc_x ub_rs in
          let ub_rs = add_to_ub_rs x new_rc_x ub_rs in
          rs, ub_rs
        end
      )uses_p (env.rs, ub_rs)
  in
  let ub_rs = MD.remove p ub_rs in
  {env with rs = MD.add p (v,dv,ex) rs; ub_rs=ub_rs }

let solve_eq env (px, n, ex) =
  (*Debug.solve_eq px n ex;*)
  let rpx, z, ex = env_nform env (px, Z.one, ex) in
  let n = Z.mult n z in
  let rp = C.arith_of_alien rpx in
  let rp = {rp with c = Z.sub rp.c n} in
  if MD.is_empty rp.m then
    if Z.is_zero rp.c then env
    else raise (Ex.Inconsistent ex)
  else
    let p, v, dv = solve_rat rp in
    (*Timer.Fm_simplex_apply_mapping_to_rs.start();*)
    let env = apply_mapping_to_rs p v dv ex env in
    let env = {env with pending_rs = p :: env.pending_rs} in
    (*Timer.Fm_simplex_apply_mapping_to_rs.stop();*)
    env
