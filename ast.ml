(******************************************************************************)
(** Ce module contient les structures de donn�es utilis�es lors du parsing, 
    du typage et du preprocessing **)
(******************************************************************************)
open Format

type logic    = Qf_lia | Unknown_logic
type status   = Sat  | Unsat | Unknown
type spec_cst = Decimal | Numeral | String | Hexa | Binary
type ty       = Int | Bool | Dummy
type op = Add | Sub | Mult | Le | Lt | Ge | Gt | Eq | Not | And | Or | Ite

type expr = { mutable ty : ty ; mutable desc : desc; nb_ite : int }
and desc =
  | True
  | False
  | Const of string * spec_cst
  | Var   of string 
  | Let   of (string * expr) list * expr
  | App   of op * expr list


let rec is_simple t = 
  match t.desc with
    | App((Le|Lt|Ge|Gt|Eq|Not|And|Or| Ite),_) -> false
    | _ -> t.nb_ite = 0
  

(*
let rec is_simple t = match t.desc with
  | App((Add|Sub|Mult),_) | Const _ | Var _ | True | False -> 
    fprintf Options.fmt "nb_ite = %d@." t.nb_ite;
    true
  | _ -> false
*)

(*
  | App((Not|And|Or|Eq|Ite),_) | Let _  -> false
*)


(*
let rec is_simple t = match t.desc with
  | Const _ -> true
  | App((Add|Sub|Mult),l) -> true
  | _ -> false
*)

let str_of_logic = function 
    Qf_lia -> "QF_LIA" | _ -> assert false

let str_of_status = function  
    Sat -> "sat" | Unsat -> "unsat" | Unknown -> "unknown"

let str_of_type = function 
    Int    -> "Int" | Bool   -> "Bool" | Dummy  -> "Dummy"

let str_of_op = function
  | Add  -> "+" | Sub  -> "-" | Mult -> "*" 
  | Ge   -> ">=" | Gt   -> ">" | Le   -> "<=" | Lt   -> "<" | Eq   -> "="
  | Not  -> "not" | And  -> "and" | Or   -> "or" 
  | Ite  -> "ite"
      
let cpt = ref 0

let indent () = 
  let s = ref "" in
  for i = 0 to !cpt do s := !s ^ "  " done; !s

let rec print fmt t =
  match t.desc with
    | Const (s,k) -> fprintf fmt "%s" s
    | Var s       -> fprintf fmt "%s" s
    | App (op,tl) -> fprintf fmt "(%s %a)" (str_of_op op) pexpr_list tl
    | True        -> fprintf fmt "true"
    | False       -> fprintf fmt "false"
    | Let (vbl,in_t) -> 
      fprintf fmt "(let (";
      List.iter (fun (v,exp) -> fprintf fmt "(%s %a) " v print exp) vbl;
      fprintf fmt ") %a)" print in_t
        
and pexpr_list fmt tl = List.iter (fprintf fmt " %a" print) tl
