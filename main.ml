open Format
open Lexing
open Options
open Ast_translator
open Sat_solver

(* fonction de debug d'affichage des stats a la fin *************************)
let print_stats () = 
  let cpu_time = Timer.General.get() in
  eprintf "===============================================================@.";
  eprintf "restarts                   : %-12l@." env.starts;
  eprintf "conflicts                  : %-12l   (%.0f /sec)@."
    env.conflicts ((to_float env.conflicts) /. cpu_time);
  eprintf "decisions                  : %-12l   (%.0f /sec)@."
    env.decisions ((to_float env.decisions) /. cpu_time);
  eprintf "propagations               : %-12l   (%.0f /sec)@."
    env.propagations ((to_float env.propagations) /. cpu_time);
  eprintf "conflict literals          : %-12l   (%4.2f %% deleted)@."
    env.tot_literals 
    ((to_float (env.max_literals - env.tot_literals)) *. 100. /. 
       (to_float env.max_literals));
  eprintf "CPU time                   : %g s@." cpu_time;
  eprintf "===============================================================@.";
  eprintf "CPU time in Simplex.main                         : %g s@."
    (Timer.Simplex_main.get());
  eprintf "CPU time in CC_assume                            : %g s@." 
    (Timer.CC_assume.get());
  eprintf "CPU time in CC_expensive_processing              : %g s@." 
    (Timer.CC_expensive_processing.get());
  eprintf "CPU time in Solver.minimize_dep                  : %g s@." 
    (Timer.Solver_minimize_dep.get());
  eprintf "CPU time in Fm_simplex_split_constrs             : %g s@." 
    (Timer.Fm_simplex_split_constrs.get());
  eprintf "CPU time in Fm_simplex_apply_mapping_to_rs       : %g s@." 
    (Timer.Fm_simplex_apply_mapping_to_rs.get());
  eprintf "CPU time in Fm_simplex_apply_mapping_to_bounds_2 : %g s@." 
    (Timer.Fm_simplex_apply_mapping_to_bounds_2.get());
  eprintf "CPU time in Fm_simplex_clustering                : %g s@." 
    (Timer.Fm_simplex_clustering.get());
  eprintf "CPU time in Tp_apply_mapping                     : %g s@." 
    (Timer.Tp_apply_mapping.get());

  eprintf "===============================================================@."

let _ = 
  Sys.set_signal Sys.sigint 
    (Sys.Signal_handle 
       (fun _ ->
          print_stats ();
          fprintf fmt "@.>.>.>.>.>.>. USER WANTS ME TO STOP .<.<.<.<.<.<@.@."; 
          exit 1))

let check_pp_only func arg v =
  if !pp_only - v = 0 then raise Exit;
  if !pp_only + v = 0 then (func arg; raise Exit)

let preprocess () = 
  let cin = match !Options.file with Some f -> open_in f | None   -> stdin in
  
  (* 1. Parsing *)
  Parser.parse Lexer.token (from_channel cin); 
  close_in cin;
  Dsteps.step_parsing();
  check_pp_only Env.output_parsed_ast () 1;

  (* 2. Typing *)
  Typechecker.type_assertions ();   
  Dsteps.step_typing();
  check_pp_only Env.output_typed_ast () 2;
  
  (* 3.x Conversion from AST to ..... CNF *)
  let f = NhForm.form_of_assertions Env.assertions in
  Queue.clear Env.assertions;
  Dsteps.step_conversion "Ast to non hashed formula";
  check_pp_only (Env.output_non_hashed_form NhForm.print) f 3;
  
  let f = Cnf_translator.hashed_form f in
  Dsteps.step_conversion "Hashed normalized formula";
  check_pp_only (Env.output_hashed_form Cnf_translator.print) f 4;
  
  let f = Cnf_translator.early_pruning f in
  Dsteps.step_conversion "Early pruning of the formula";
  check_pp_only (Env.output_hashed_form Cnf_translator.print) f 5;
  
  let unit, non_unit, proxies = Cnf_translator.cnf f in
  Dsteps.step_conversion "CNF";
  check_pp_only (fun () -> Env.output_cnf unit non_unit proxies) () 6;
  
  unit, non_unit

exception Out of int

let sort_ll ll = 
  List.fast_sort
    (fun l1 l2 ->
       let c = List.length l2 - List.length l1 in
       if c <> 0 then c 
       else
         try
           List.iter2
             (fun a b -> 
                let c = Stypes.cmp_atom b a in
                if c <> 0 then raise (Out c)
             )l1 l2;
           0
         with Out c -> c
    )ll
    
let int_and_call_sat_solver unit non_unit =
  let unit = sort_ll unit in
  let non_unit = sort_ll non_unit in
      
  eprintf "=======================[ Problem Statistics ]===================@.";
  eprintf "@.";
  let nbv = Vec.size Stypes.vars in
  let nb_u_c  = List.length unit in
  let nb_nu_c = List.length non_unit in
  eprintf "  Number of variables:     %-12d @." nbv;
  eprintf "  Number of u-clauses:     %-12d @." nb_u_c;
  eprintf "  Number of nu-clauses:    %-12d @." nb_nu_c;
  
  env.vars  <- Vec.copy Stypes.vars;
  env.order <- Heap.init nbv;
  begin try while true do ignore(pick_branch_lit ()) done with Sat -> () end;
  env.nb_init_vars <- nbv;
  Vec.grow_to env.model nbv;
  Vec.grow_to env.clauses nb_nu_c;
  Vec.grow_to env.learnts nb_nu_c;
  env.nb_init_clauses <- nb_nu_c;
  env.tenv <- T.init Stypes.vars;
  add_clauses unit;
  add_clauses non_unit;
  solve()

let _ = 
  try
    let unit, non_unit = preprocess () in
    match unit, non_unit with
      | [[a]], [] when Stypes.eq_atom Stypes.vrai_atom a -> raise Sat
      | [[a]], [] when Stypes.eq_atom Stypes.faux_atom a -> raise Unsat
      | _ -> 
          let clusters = Clustering.main unit non_unit in
          List.iter
            (fun (unit, non_unit) ->
               fprintf fmt "new cluster (%d u and %d n-u)@."
                 (List.length unit) (List.length non_unit);
               let unit, non_unit = Learning.main unit non_unit in
               reset_refs ();
               try int_and_call_sat_solver unit non_unit
               with Sat -> fprintf fmt "cluster is sat@."
            )clusters;
          raise Sat
  with 
    | Exit -> 
      printf ";;expected answer: %s@." (Ast.str_of_status !Env.status);
      print_stats();
      exit 0
    | Sat ->
      if !Env.status = Ast.Unsat then begin
        fprintf fmt "Warning: the status is unsat but the solver says sat!@.";
        if !warnings_as_errors then exit 1
      end;
      printf "sat@.";
      print_stats();
        exit 0
    | Unsat ->
        if !Env.status = Ast.Sat then begin
          fprintf fmt "Warning: the status is sat but the solver says unsat!@.";
          (*if !warnings_as_errors then*)
          assert false (*exit 1*)
        end;
        printf "unsat@.";
        print_stats();
        exit 0
