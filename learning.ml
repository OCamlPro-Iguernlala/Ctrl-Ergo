open Numbers
open Ast_translator
open Format
open Options
open Stdlib
module Ex = Explanation

module Arith = struct
  module C = Combine
  module M = C.MDec
  module MZ = Map.Make(Z)  
  module SE = Set.Make(Explanation)

  module SC = struct
    exception Out of int

    include Set.Make
      (struct
        type t = Stypes.atom list
        let compare a b = 
          try
            List.iter2
              (fun a b -> 
                let c = Stypes.cmp_atom a b in
                if c <> 0 then raise (Out c)
              )a b ;
            0
          with Out c -> c
            | Invalid_argument _ -> List.length a - List.length b
       end)

    let ajouter c s = 
      let c = List.fast_sort Stypes.cmp_atom c in
      add c s
  end

(*
  module SC = struct
    let empty = []
    let ajouter c s = c :: s
    let elements s = s
    let cardinal s = List.length s
  end
*)

  let print_mp mp = 
    if !dlearning then begin
      fprintf fmt "## MP's content: ########################################@.";
      M.iter
        (fun r (eq,dis,lt,geq) ->
          fprintf fmt "@.============================================@.";
           fprintf fmt "info of %a@." C.print r;
           fprintf fmt "EQ:@.";
           MZ.iter (fun c (a,na) -> 
             fprintf fmt "  %s@." (Z.to_string c);
             fprintf fmt "      %a@." Ex.print (Ex.singleton a)
           ) eq;
           fprintf fmt "@.DISEQ: " ;
           MZ.iter (fun c (_,_) -> fprintf fmt "  %s " (Z.to_string c)) dis;
           fprintf fmt "@.LT: ";
           MZ.iter (fun c (_,_) -> fprintf fmt "  %s " (Z.to_string c)) lt;
           fprintf fmt "@.GEQ: ";
           MZ.iter (fun c (_,_) -> fprintf fmt "  %s " (Z.to_string c)) geq;
        )mp;
      fprintf fmt "@.";
    end

  let print_mpp mpp = 
    if !dlearning then begin
      fprintf fmt "## MPP's content: #######################################@.";
      M.iter
        (fun r (eq,_) ->
          fprintf fmt "@.============================================@.";
          fprintf fmt "info of %a@." C.print r;
          fprintf fmt "EQ:@.";
          MZ.iter 
            (fun c se -> 
              fprintf fmt "  %s@." (Z.to_string c);
              SE.iter (fprintf fmt "      %a@." Ex.print) se;
            )eq;
          
          (*fprintf fmt "@.DISEQ: " ;
          MZ.iter (fun c se -> fprintf fmt "  %s " (Z.to_string c)) dis;
          fprintf fmt "@.LT: ";
          MZ.iter (fun c se -> fprintf fmt "  %s " (Z.to_string c)) lt;
          fprintf fmt "@.GEQ: ";
          MZ.iter (fun c se -> fprintf fmt "  %s " (Z.to_string c)) geq;*)
        )mpp;
      fprintf fmt "@.";
    end


  (*################################################################*)

  let merge se1 se2 = 
    SE.fold
      (fun ex acc ->
        SE.fold 
          (fun ex' acc -> 
            if Ex.is_empty (Ex.inter ex ex') then 
              SE.add (Ex.union ex ex') acc
            else acc
        ) se2 acc) 
      se1 SE.empty

  let compute_reducts mpp x c l =
    if !dlearning then 
      fprintf fmt "@.--- leaf %a -------------------@." C.print x;
    try
      if List.length l > 10000 then raise Not_found;
      let eqs, _ = M.find x mpp in
      List.fold_left
        (fun acc (r, exr_st) ->
          MZ.fold
            (fun z exz_st acc ->
              let y = Arith {m=M.empty; c=z} in
              let r' = C.subst x y r in
              let ex_st' = merge exr_st exz_st in
              if !dlearning then begin
                fprintf fmt "%a --> %a@." C.print x C.print y;
                fprintf fmt "%a devient %a@." C.print r C.print r';
                fprintf fmt "explications:@.";
                SE.iter (fprintf fmt "   %a@." Ex.print) ex_st';
              end;
              (r', ex_st') :: acc
            )eqs acc
        )[] l
    with Not_found -> l
    
  let ded_acc = ref SC.empty

  let add_equality r ser z sez mpp = 
    let se = merge ser sez in
    if !dlearning then 
      fprintf fmt 
        "@.apprentissage de %a = %s grace a:@." C.print r (Z.to_string z);
    
    if !dlearning then SE.iter (fprintf fmt "     %a@." Ex.print) se;
    if !dlearning then SE.iter (fprintf fmt "ex_r=     %a@." Ex.print) ser;
    if !dlearning then SE.iter (fprintf fmt "ex_z=     %a@." Ex.print) sez;
    if SE.compare ser sez = 0 then begin
      if !dlearning then fprintf fmt "TRIVIAL: a -> a@.";
      mpp
    end
    else
      let zr = Arith {m=M.empty; c=z} in
      let a = Arith.mk_literal Ast.Eq r zr in
      if !dlearning then fprintf fmt "normal_form %a@." Stypes.pr_atom a;
      match a.Stypes.is_neg, a.Stypes.var.Stypes.view with
        | false, Stypes.EQ(t,z) ->
          (*
          let eq, geq = M.find_default t (MZ.empty, MZ.empty) mpp in
          let se = SE.union se (MZ.find_default z SE.empty eq) in
          let eq = MZ.add z se eq in
          (*fprintf fmt "normal_form %a@." Stypes.pr_atom a;
            SE.iter (fprintf fmt "     %a@." Ex.print) se;*)
          M.add t (eq,geq) mpp*)
            mpp

        | true, Stypes.TRUE -> 
          SE.iter
            (fun ex ->
              let clause = Ex.fold (fun a l -> a.Stypes.opp :: l) ex [] in
              if !dlearning then begin
                fprintf fmt "=========================================@.(or";
                List.iter (fprintf fmt " %a" Stypes.pr_atom) clause;
                fprintf fmt ")@.=========================================@.";
              end;
              ded_acc := SC.ajouter clause !ded_acc;
            )se;
          mpp

        | false, Stypes.TRUE ->
          SE.iter
            (fun ex ->
              let l = Ex.fold (fun a l -> a.Stypes.opp :: l) ex [] in
              let rec f todo don acc =
                match todo with
                  | [] -> acc
                  | a::todo ->
                    let clause = a.Stypes.opp::(List.rev_append todo don) in
                    ded_acc := SC.ajouter clause !ded_acc;
                    f todo (a::don) (clause::acc)
              in
              let lclauses = f l [] [] in
              if !dlearning then 
              List.iter
                (fun clause ->
                  fprintf fmt "=========================================@.(or";
                  List.iter (fprintf fmt " %a" Stypes.pr_atom) clause;
                  fprintf fmt ")@.=========================================@.";
                )lclauses;
            )se;
          mpp

        | _ -> assert false
        


  let add_learnt eq mpp reducts = 
    List.fold_left
      (fun mpp (r,ser) -> MZ.fold (add_equality r ser) eq mpp) mpp reducts


  let apply_subst r eq _ mpp =
    let p = C.arith_of_alien r in
    (*match r with
      | Leaf _ | Match _ -> mpp
      | Arith p ->*)
    if !dlearning then 
      fprintf fmt "@.--- reducts de %a -------------------@." C.print r;
    
    let reducts = 
      M.fold (compute_reducts mpp) p.m [r, SE.singleton Ex.empty] in
    if !dlearning then 
      fprintf fmt "--- apprentissage des reducts dee %a ----@." C.print r;
    add_learnt eq mpp reducts
    

  let learn_by_subst mpp =
    M.fold (fun r (eq, geq) mpp -> apply_subst r eq geq mpp) mpp mpp
    
  (*################################################################*)
  

  let dispatch mp a r c case =
    let eq, dis, lt, geq = 
      try M.find r mp with Not_found ->(MZ.empty,MZ.empty,MZ.empty,MZ.empty) in
    let na = a.Stypes.opp in
    let eq, dis, lt, geq = match case with
      | 1 -> MZ.add c (a,na) eq, dis, lt, geq
      | 2 -> eq, MZ.add c (a,na) dis, lt, geq
      | 3 -> eq, dis, MZ.add c (a,na) lt, geq
      | 4 -> eq, dis, lt, MZ.add c (a,na) geq
      | _ -> assert false
    in 
    M.add r (eq, dis, lt, geq) mp

  let add_aux mp a = 
    (*fprintf fmt "add_aux: %a@." Stypes.pr_atom a;*)
    match a.Stypes.var.Stypes.view, a.Stypes.is_neg with
      | Stypes.EQ(r,c), false -> dispatch mp a r c 1
      | Stypes.EQ(r,c), true  -> dispatch mp a r c 2
      | Stypes.GE(r,c), true  -> dispatch mp a r c 3
      | Stypes.GE(r,c), false -> dispatch mp a r c 4
      | _ -> mp
        
  let add_atom mp a = add_aux (add_aux mp a) a.Stypes.opp
    
  let elements s = MZ.fold (fun c (a,b) l -> (c,a,b)::l) s []
    
  let learn_from_eq eq acc = 
    let rec f l acc = 
      match l with
        | [] | [_] -> acc
        | (x,ax,nax) :: l ->
            let acc = 
              List.fold_left
                (fun acc (y,ay,nay) ->
                   if !dlearning then 
                     fprintf fmt "learn_from_eq: %a or %a@."
                       Stypes.pr_atom nax Stypes.pr_atom nay;
                [nax; nay] :: acc
                )acc l
            in
            f l acc
    in
    f (elements eq) acc
  
  let learn_from_dis dis acc = acc

  let learn_from_lt lt acc = 
    let rec f l acc = 
      match l with
        | [] | [_] -> acc
        | (x,ax,nax) :: l ->
            let acc = 
              List.fold_left
                (fun acc (y,ay,nay) ->
                   if !dlearning then 
                     fprintf fmt "learn_from_lt: %a or %a@."
                       Stypes.pr_atom ax Stypes.pr_atom nay;
                   [ax; nay] :: acc
                )acc l
            in
            f l acc
    in
    f (elements lt) acc

  let learn_from_geq geq acc = 
    let rec f l acc = 
      match l with
        | [] | [_] -> acc
        | (x,ax,nax) :: l ->
            let acc = 
              List.fold_left
                (fun acc (y,ay,nay) ->
                  if !dlearning then
                     fprintf fmt "learn_from_geq : %a or %a@."
                       Stypes.pr_atom nax Stypes.pr_atom ay;
                  [ay; nax] :: acc
                )acc l
            in
            f l acc
    in
    f (elements geq) acc

  let learn_from_eq_dis eq dis acc = acc

  let learn_from_eq_lt eq lt acc = 
    MZ.fold
      (fun x (ax,nax) acc ->
         MZ.fold
           (fun y (ay,nay) acc ->
              if Z.compare x y >= 0 then begin (* VERIF *)
                if !dlearning then
                  fprintf fmt "learn_from_eq_lt (THEN): %a or %a@." 
                    Stypes.pr_atom nax Stypes.pr_atom nay;
                [nax; nay] :: acc
              end
              else begin
                if !dlearning then
                  fprintf fmt "learn_from_eq_lt (ELSE): %a or %a@." 
                    Stypes.pr_atom nax Stypes.pr_atom ay;
                [nax; ay] :: acc
              end
           )lt acc
      )eq acc

  let learn_from_eq_geq eq geq acc =
    MZ.fold
      (fun x (ax,nax) acc ->
         MZ.fold
           (fun y (ay,nay) acc ->
              if Z.compare x y < 0 then begin (* VERIF *)
                if !dlearning then
                  fprintf fmt "learn_from_eq_geq (THEN): %a or %a@." 
                    Stypes.pr_atom nax Stypes.pr_atom nay;
                [nax; nay] :: acc
              end
              else begin
                if !dlearning then 
                  fprintf fmt "learn_from_eq_geq (ELSE): %a or %a@." 
                    Stypes.pr_atom nax Stypes.pr_atom ay;
                [nax; ay] :: acc
              end
           )geq acc
      )eq acc


  let learn_from_eq_geq2 eq geq acc =
    MZ.fold
      (fun x se acc ->
        SE.fold
          (fun ex acc ->
            let nex = Ex.fold (fun a acc -> a.Stypes.opp::acc) ex [] in
            MZ.fold
              (fun y (ay,nay) acc ->
                if Z.compare x y < 0 then begin (* VERIF *)
                  if !dlearning then
                    fprintf fmt "learn_from_eq_geq (THEN): not (%a) or %a@." 
                      Ex.print ex Stypes.pr_atom nay;
                  (nay::nex) :: acc
                end
                else begin
                  if !dlearning then 
                    fprintf fmt "learn_from_eq_geq (ELSE): not (%a) or %a@." 
                      Ex.print ex Stypes.pr_atom ay;
                  (ay::nex) :: acc
                end
              )geq acc
          )se acc
      )eq acc



  let learn_from_dis_lt dis lt acc = 
    MZ.fold
      (fun x (ax,nax) acc ->
         MZ.fold
           (fun y (ay,nay) acc ->
              if Z.compare x y >= 0 then begin (* VERIF *)
                if !dlearning then 
                  fprintf fmt "learn_from_dis_lt: %a or %a@." 
                    Stypes.pr_atom ax Stypes.pr_atom nay;
                [ax; nay] :: acc 
              end
              else acc
           )lt acc
      )dis acc

  let learn_from_dis_geq dis geq acc = 
    MZ.fold
      (fun x (ax,nax) acc ->
         MZ.fold
           (fun y (ay,nay) acc ->
              if Z.compare x y < 0 then begin (* VERIF *)
                if !dlearning then 
                  fprintf fmt "learn_from_dis_geq: %a or %a@." 
                    Stypes.pr_atom ax Stypes.pr_atom nay;
                [ax; nay] :: acc 
              end
              else acc
           )geq acc
      )dis acc

  let learn_from_lt_geq lt geq acc =
    MZ.fold
      (fun x (ax,nax) acc -> (* r <= x i.e. x is an upper bound *)
         MZ.fold
           (fun y (ay,nay) acc -> (* r >= y i.e. y is a lower bound *)
              if Z.compare y x > 0 then begin (* VERIF *)
                if !dlearning then 
                  fprintf fmt "learn_from_lt_geq: %a or %a@."
                    Stypes.pr_atom nax Stypes.pr_atom nay;
                [nax; nay] :: acc
              end
              else acc
           )geq acc
      )lt acc


  let learn_by_cross mp mpp non_unit =
    M.fold
      (fun r (eq,dis,lt,geq) acc ->
        (* learn_from_lt et learn_from_geq : kif kif  (1) *)
        (* learn_from_eq_lt et learn_from_eq_geq : kif kif  (2) *)
        let acc = learn_from_eq eq acc in
        let acc = learn_from_dis dis acc in (*dummy*)
        let acc = learn_from_lt lt acc in   (* (1) *)
        let acc = learn_from_geq geq acc in (* (1) *)
        let acc = learn_from_eq_dis eq dis acc in (*dummy*)
        let acc = learn_from_eq_lt eq lt acc in   (* (2) *)
        let acc = learn_from_eq_geq eq geq acc in (* (2) *)

        (*
        let acc = 
          try learn_from_eq_geq2 (fst (M.find r mpp)) geq acc
          with Not_found -> assert false in
        *)
        
(*
  fprintf fmt ">>>>>>>>>>%a@." C.print r;
        fprintf fmt "EQ:@.";
        MZ.iter (fun c (a,na) -> 
          fprintf fmt "  %s@." (Z.to_string c);
          fprintf fmt "      %a@." Ex.print (Ex.singleton a)
        ) eq;
        
        let l1 = learn_from_eq_geq eq geq [] in
        let l2 = 
          try 
            let eq, _ = M.find r mpp in

            fprintf fmt "EQ:@.";
            MZ.iter 
              (fun c se -> 
                fprintf fmt "  %s@." (Z.to_string c);
                SE.iter (fprintf fmt "      %a@." Ex.print) se;
              )eq;

            learn_from_eq_geq2 eq geq []
          with Not_found -> assert false in

        fprintf fmt "L1:%d@." (List.length l1);
        fprintf fmt "L2:%d@." (List.length l2);
        assert (List.length l1 = List.length l2);
*)

        
        (* certaines clauses de learn_from_dis_lt sont dans 2 ? *)
        let acc = learn_from_dis_lt dis lt acc in
        
        (* certaines clauses de learn_from_dis_geq sont dans 2 ? *)
        let acc = learn_from_dis_geq dis geq acc in
        let acc = learn_from_lt_geq lt geq acc in
        acc
      ) mp non_unit
      
  
  let diseqs_as_ineqs unit non_unit =
    List.fold_left
      (fun acc uc ->
         match uc with
           | [] | _::_::_ -> assert false
           | [a] ->
               match a.Stypes.var.Stypes.view, a.Stypes.is_neg with
                 | Stypes.EQ(t,z), true  -> 
                   let g = 
                       Stypes.mk_atom 
                         (Stypes.GE(t, Z.add z Z.one)) false in
                     let l = Stypes.mk_atom (Stypes.GE(t, z)) true in
                     if !dlearning then begin
                       fprintf fmt "unit: %a@." Stypes.pr_atom a;
                       fprintf fmt "donc: %a or %a@." 
                         Stypes.pr_atom g Stypes.pr_atom l
                     end;
                     [g; l] :: acc
                 | _ -> acc
      )non_unit unit

  let mp_plus_of_mp mp =
    M.map
      (fun (eq, _, _, geq) ->
        MZ.map (fun (a,_) -> SE.singleton (Ex.singleton a)) eq,
        MZ.map (fun (a,_) -> SE.singleton (Ex.singleton a)) geq
      )mp
      

  let learn unit non_unit = 
    fprintf fmt "# nb_unit     %d@." (List.length unit);
    let nu = List.length non_unit in
    fprintf fmt "# nb_non_unit %d@." nu;
    (*let non_unit = diseqs_as_ineqs unit non_unit in*)
    let mp = List.fold_left (List.fold_left add_atom) M.empty unit in
    let mp = List.fold_left (List.fold_left add_atom) mp non_unit in
    print_mp mp;

    let mpp = mp_plus_of_mp mp in
    let mpp = learn_by_subst mpp in
    print_mpp mpp;


    let non_unit2 = learn_by_cross mp mpp non_unit in
    fprintf fmt "# nb_learnt %d@." (List.length non_unit2 - nu);
    let s = List.fold_left (fun s c -> SC.ajouter c s) !ded_acc non_unit2 in
    fprintf fmt "# nb_learnt-II %d@." (SC.cardinal !ded_acc);
    unit, (SC.elements s)
      (*unit, (List.rev_append (SC.elements !ded_acc) non_unit2)*)

end

let main unit non_unit =
  if !Options.learning then
    let res = Arith.learn unit non_unit in
    Dsteps.step_learning ();
    if !learning_only then raise Exit;
    res
  else unit, non_unit
