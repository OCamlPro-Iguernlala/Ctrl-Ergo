(******************************************************************************)
(** Ce module fournit la structure hashcons�e de formules sur des valeurs 
    s�mantiques **)
(******************************************************************************)

open Ast_translator
open Stypes
open Options
open Format
module T = Theory
module Ex = Explanation

type view = 
  | UNIT of atom
  | AND  of t list
  | OR   of t list

and t = { node : view ; tag : int }

module HC = 
  Hashcons.Make
    (struct
      type t' = t
      type t  = t'
          
      let tag tag f = { f with tag = tag }
        
      let equal f1 f2 = 
        let eq_aux c1 c2 = match c1, c2 with
          | UNIT x , UNIT y -> eq_atom x y
          | AND u  , AND v | OR u , OR v  -> 
            (try
               List.iter2
                 (fun x y -> if x.tag <> y.tag then raise Exit) u v; true
             with Exit | Invalid_argument "List.iter2" -> false)
          | _, _ -> false
	in
        eq_aux f1.node f2.node
          
      let hash f = 
        let h_aux f = match f with
          | UNIT a -> h_atom a
          | AND l  -> List.fold_left (fun acc f -> acc * 19 + f.tag) 1 l
          | OR l   -> List.fold_left (fun acc f -> acc * 23 + f.tag) 1 l
        in  
        let h = h_aux f.node in
        match f.node with
          | UNIT _ -> abs (3 * h)
          | AND _  -> abs (3 * h + 1)
          | OR _   -> abs (3 * h + 2)
            
     end)

let cpt = ref 0

let sp() = let s = ref "" in for i = 1 to !cpt do s := " " ^ !s done; !s ^ !s

let ptag fmt a = if !dtags then fprintf fmt "[F-Tag:%d]" a.tag 

let pval fmt a = 
  fprintf fmt "%s"
    (if a.is_true then "[1]" else if a.opp.is_true then "[0]" else "")

let rec print fmt fa = match fa.node with
  | UNIT a -> fprintf fmt "%a%a%a" pr_atom a pval a ptag fa
  | AND s  -> 
    incr cpt;
    fprintf fmt "(and%a" print_list s;
    decr cpt;
    fprintf fmt "@.%s)%a" (sp()) ptag fa
      
  | OR s   -> 
    incr cpt;
    fprintf fmt "(or%a" print_list s;
    decr cpt;
    fprintf fmt "@.%s)%a" (sp()) ptag fa
      
and print_list fmt l =
  match l with
    | [] -> assert false
    | e::l ->
      List.iter(fprintf fmt "@.%s%a " (sp()) print) l;
      fprintf fmt "@.%s%a" (sp()) print e

let print fmt f = cpt := 0; print fmt f

let print_stats fmt = ()

let compare f1 f2 = f1.tag - f2.tag  
let equal   f1 f2 = f1.tag - f2.tag = 0
let hash f = f.tag
let tag  f = f.tag
let view f = f.node

let make pos = HC.hashcons {node=pos ; tag= -1 (*dump*)}

let complements f1 f2 = match f1.node, f2.node with
  | UNIT a, UNIT b -> a.aid - b.opp.aid = 0
  | _ -> false

let mk_lit a = make (UNIT a)
let vrai = make (UNIT vrai_atom)
let faux = make (UNIT faux_atom)

let compare2 f1 f2 = match f1.node, f2.node with
  | UNIT a, UNIT b -> cmp_atom a b
  | UNIT _, _ -> -1
  | _, UNIT _ -> 1
  | _ -> compare f1 f2
    

let merge_and_check l1 l2 =
  let rec merge_rec l1 l2 hd =
    match l1, l2 with
      | [], l2 -> l2
      | l1, [] -> l1
      | h1 :: t1, h2 :: t2 ->
        let c = compare2 h1 h2 in
        if c = 0 then merge_rec l1 t2 hd
        else 
          if compare2 h1 h2 < 0
          then begin
            if complements hd h1 then raise Exit;
            h1 :: merge_rec t1 l2 h1
          end
          else begin
            if complements hd h2 then raise Exit;
            h2 :: merge_rec l1 t2 h2
          end
  in
  match l1, l2 with
    | [], l2 -> l2
    | l1, [] -> l1
    | h1 :: t1, h2 :: t2 ->
      let c = compare2 h1 h2 in
      if c = 0 then merge_rec t1 l2 h1
      else 
        if compare2 h1 h2 < 0
        then merge_rec l1 l2 h1
        else merge_rec l1 l2 h2

let mk_or l = 
  try 
    let so, nso = 
      List.fold_left
        (fun ((so,nso) as acc) e -> 
          match e.node with 
            | OR l  -> merge_and_check so l, nso
            | UNIT a-> 
              if a.is_true then raise Exit;
              if a.opp.is_true then acc
              else so, e::nso
            | _     -> so, e::nso
        )([],[]) l
    in 
    let delta_inv = List.fast_sort (fun a b -> compare2 b a) nso in
    let delta_u = match delta_inv with
      | [] -> delta_inv
      | e::l ->
        let _, delta_u = 
          List.fold_left
            (fun ((c,l) as acc) e ->
              if complements c e then raise Exit;
              if equal c e then acc
              else (e, e::l)
            )(e,[e]) l
        in 
        delta_u
    in
    match merge_and_check so delta_u with
      | [] -> faux
      | [e]-> e
      | l  -> make (OR l)
  with Exit -> vrai

let mk_and l = 
  try 
    let so, nso = 
      List.fold_left
        (fun ((so,nso) as acc) e -> 
          match e.node with 
            | AND l -> merge_and_check so l, nso
            | UNIT a-> 
              if a.opp.is_true then raise Exit;
              if a.is_true then acc
              else so, e::nso
            | _     -> so, e::nso
        )([],[]) l
    in 
    let delta_inv = List.fast_sort (fun a b -> compare2 b a) nso in
    let delta_u = match delta_inv with
      | [] -> delta_inv
      | e::l ->
        let _, delta_u = 
          List.fold_left
            (fun ((c,l) as acc) e ->
              if complements c e then raise Exit;
              if equal c e then acc
              else (e, e::l)
            )(e,[e]) l
        in 
        delta_u
    in
    match merge_and_check so delta_u with
      | [] -> vrai
      | [e]-> e
      | l -> make (AND l)
  with Exit -> faux


let rec hform_aux f = match NhForm.view f with
  | NhForm.UNIT a -> mk_lit a
  | NhForm.AND l  -> mk_and (List.rev_map (hform_aux) l)
  | NhForm.OR l   -> mk_or  (List.rev_map (hform_aux) l)
  | NhForm.IFF (f1,f2) -> 
    hform_aux (NhForm.mk_and [NhForm.mk_or [NhForm.mk_not f1; f2]; 
                              NhForm.mk_or [NhForm.mk_not f2; f1]])
      
let hashed_form f = hform_aux f

  (*** Early BCP ***)
  
module MD = Combine.MDec
module RS = MD

let print_rs env = T.print env

let assume_in_rs env a = T.light_assume env a
    
let simplify_atom env a =
  if !debug then print_rs env;
  let b = 
    match a.is_neg, a.var.view with
      | false, EQ(t,z) -> 
        let t, _ = T.env_nform env (t, Ex.empty) in
        Arith.mk_literal Ast.Eq t (Arith {m=MD.empty; c=z})
      | true, EQ(t,z) -> 
        let t, _ = T.env_nform env (t, Ex.empty) in
        let b = Arith.mk_literal Ast.Eq t (Arith {m=MD.empty; c=z}) in
        b.opp
          
      | false, GE(t,z) -> 
        let t, _ = T.env_nform env (t, Ex.empty) in
        Arith.mk_literal Ast.Ge t (Arith {m=MD.empty; c=z})

      | true, GE(t,z) -> 
        let t, _ = T.env_nform env (t, Ex.empty) in
        let b = Arith.mk_literal Ast.Ge t (Arith {m=MD.empty; c=z}) in
        b.opp
          
      | _ -> a
  in
  if not (eq_atom a b) && !debug then
    fprintf fmt "%a --> %a@.@." pr_atom a pr_atom b;
  b


let assume_eqs rs l =
  let rs, eqs, todo, others =
    List.fold_left
      (fun (rs,eqs,todo,others) f -> 
        match f.node with
          | UNIT a ->
            begin match a.is_neg, a.var.view with
              | false, EQ _ ->
                if a.opp.is_true then raise Unsolvable;
	        if !debug then fprintf fmt "assume_eq  %a@." pr_atom a;
                let a = simplify_atom rs a in
                if a.opp.is_true then raise Unsolvable;
                if !debug then fprintf fmt "assume_eq' %a@." pr_atom a;
                if not a.is_true then
                  let rs = 
                    try assume_in_rs rs a 
                    with Ex.Inconsistent _ -> raise Unsolvable 
                  in
                  a.is_true <- true;
                  rs, (mk_lit a)::eqs, todo, others
                else rs, eqs,todo, others (* XXX deja vrai donc a enlever ?*)
              | _ -> rs, eqs, f::todo, others
            end
          | _ -> rs, eqs, todo, f::others
      )(rs,[],[],[]) l
  in
  rs, eqs, todo, others

let assume_list rs l =
  let rs, eqs, todo, others = assume_eqs rs l in
  let assumed = 
    List.fold_left
      (fun assumed f -> match f.node with
        | UNIT a ->
          if a.opp.is_true then raise Unsolvable;
          if !debug then fprintf fmt "assume  %a@." pr_atom a;
          let a = simplify_atom rs a in
          if a.opp.is_true then raise Unsolvable;
          if !debug then fprintf fmt "assume' %a@." pr_atom a;
          if not a.is_true then begin
            a.is_true <- true;
              (*let rs = assume_in_rs rs a in*)
              (*print_rs rs;*)
            (mk_lit a) :: assumed
          end
          else assumed (* XXX deja vrai donc a enlever ?*)
        | _ -> assert false
      ) eqs todo
  in
  rs, assumed, others
    
let unassume_list assumed others = 
  List.fold_left
    (fun others f -> match f.node with
      | UNIT a ->
        if not (eq_atom a vrai_atom) && not (eq_atom a faux_atom) then 
          begin
            if !debug then fprintf fmt ">unass%a@." pr_atom a;
            assert (a.is_true && not a.opp.is_true);
            a.is_true <- false;
            f :: others
          end
        else others
      | _ -> assert false
    )others assumed

let unassume_partial l  = 
  List.iter
    (fun f -> match f.node with
      | UNIT a ->
        if not (eq_atom a vrai_atom) && not (eq_atom a faux_atom) then 
          a.is_true <- false
      | _ -> ()
    )l


let rec simplify_aux rs f = match f.node with
  | AND l   -> simplify_and rs f l 
  | OR  l   -> simplify_or rs f l
  | UNIT a  -> mk_lit (simplify_atom rs a)

and simplify_and rs f l =
  if !debug then fprintf fmt "-------@.%a@.-------@." print f;
  try
    let l = List.fold_left
      (fun acc f ->
        match f.node with
          | UNIT a ->
            if a.opp.is_true then raise Exit;
            if a.is_true then acc
            else 
              let a = simplify_atom rs a in
              if a.opp.is_true then raise Exit;
              if a.is_true then acc
              else (mk_lit a) :: acc
          | _    -> f :: acc
      )[] l
    in
    match l with
      | [] -> vrai
      | l0 -> 
        try 
          let rs, assumed, others = assume_list rs l0 in
          let others = List.rev_map (simplify_aux rs) others in 
          let l = unassume_list assumed others in
          mk_and l
        with Unsolvable ->
          if !debug then 
            fprintf fmt "solve d'une atome de %a donne Unsolvable@." print f;
          unassume_partial l0;
          faux
  with Exit -> faux
    
and simplify_or rs f l =
  try
    let l = List.fold_left
      (fun acc f ->
        match f.node with
          | UNIT a ->
            if a.is_true then raise Exit;
            if a.opp.is_true then acc
            else (mk_lit (simplify_atom rs a)) :: acc
          | _      -> f :: acc
      )[] l
    in
    match l with
      | [] -> faux
      | _  -> mk_or (List.rev_map (simplify_aux rs) l)
  with Exit -> vrai
    
let cpt = ref 0
let rec pruning_aux f = 
  fprintf fmt "early-pruning round@.";
    (*fprintf fmt "%a@." print f;*)
  let f' = simplify_aux T.empty f  in
  let c = compare2 f f' in
    (*fprintf fmt "%d@." c;*)
  if c = 0 then f' else pruning_aux f'


let early_pruning f = if !early_pruning then pruning_aux f else f
    

  (* CNF conversion a la Tseitin *)
    
let acc_or = ref []
let acc_and = ref []

let rec cnf f = match f.node with
  | UNIT a -> [a]
  | AND l -> 
    List.fold_left 
      (fun acc f -> 
        match cnf f with (* retourne un OU *)
          | [] -> assert false
          | [a] -> a :: acc
          | l ->
            let proxy = 
              mk_atom (mk_proxy f.tag) false in
            acc_or := (proxy, l) :: !acc_or;
            proxy :: acc
      )[] l
      
  | OR l -> 
    List.fold_left
      (fun acc f ->
        match cnf f with (* retourne un ET *)
          | []  -> assert false
          | [a] -> a :: acc
          | l   -> 
            let proxy = 
              mk_atom (mk_proxy f.tag) false in
            acc_and := (proxy, l) :: !acc_and;
            proxy :: acc
      )[] l
      
let cnf f = 
  let acc = match f.node with
    | AND l -> List.rev_map (fun f -> cnf f) l
    | _ -> [cnf f]
  in
  let proxies = ref [] in
  let acc = 
    List.fold_left
      (fun acc (p,l) ->
        proxies := p :: !proxies;
        let np = p.opp in
        let cl, acc = 
          List.fold_left
            (fun (cl,acc) a -> (a.opp :: cl), [np; a] :: acc)([p],acc) l in
        cl :: acc
      )acc !acc_and
  in
  let acc = 
    List.fold_left
      (fun acc (p,l) ->
        proxies := p :: !proxies;
        let acc = List.fold_left (fun acc a -> [p;a.opp]::acc) acc l in
        ((p.opp) :: l) :: acc
      )acc !acc_or
  in
  let unit, non_unit = 
    List.fold_left
      (fun (u,nu) c -> 
        match c with [] -> assert false |[_] -> c::u, nu | _  -> u, c::nu)
      ([],[]) acc
  in
  unit, non_unit, !proxies
